# YAML锚点和别名的用法

##### 避免重复输入值：

```yaml
# 定义一个锚点 &list
&list 
- apple
- banana
- orange

# 使用别名 *list 引用锚点
fruits: *list
```

在上面的代码中，我们使用`&list`来定义了一个锚点，并将其中的水果列表作为值，通过别名`*list`来引用该锚点。这样可以避免在其他地方写下相同的水果列表，提高可读性。

##### 复用配置项：

```yaml
# 定义一个锚点 &dbConfig
&dbConfig
  url: jdbc:mysql://localhost:3306/demo
  username: root
  password: 123456

# 在多处引用锚点
datasource1: *dbConfig
datasource2: *dbConfig
```

在上面的代码中，我们使用`&dbConfig`来定义了一个锚点，并将其中的所有数据库连接配置项作为值，通过别名`*dbConfig`来在不同的位置上引用该锚点。这样就可以避免重复定义相同的配置项，提高了代码的可维护性。

##### 快速修改配置项：

```yaml
# 定义一个锚点 &dbConfig
&dbConfig
  url: jdbc:mysql://localhost:3306/demo
  username: root
  password: 123456
  
# 在多处引用锚点
datasource1: *dbConfig
datasource2: *dbConfig

# 修改锚点
&dbConfig
  url: jdbc:mysql://localhost:3306/mydemo
  
# 由于有别名，修改一处即可自动更新所有引用锚点的地方
```

在上面的代码中，我们定义了一个锚点`&dbConfig`，并在不同的位置上使用别名引用它。如果要修改数据库连接字符串，只需要在定义锚点处修改即可。由于所有引用的位置都是别名，所以修改一处即可自动更新所有引用锚点的地方。

##### 覆盖

```yaml
# 定义锚点 "myList"，它包含3个元素的列表
myList: &myList # &myList 表示 "myList" 是当前 YAML 文档的锚点
  - apple
  - banana
  - orange

# 使用合并键 "<<" 和引用别名 "*" 将当前层的所有键值对与 "myList" 合并
# 这里 "list" 键的值包含 "myList" 锚点下的所有键值对
list: <<: *myList
```

在上面的代码中，我们使用 `&myList` 来定义了一个锚点，并将其中的列表作为值，通过别名 `*myList` 来引用该锚点。然后，我们在 `list` 键值对中使用了合并键 `<<:` 和引用别名 `*`，负责让当前层的所有键值对和 `myList` 锚点下的数据合并，最终将它们作为 `list` 的值。