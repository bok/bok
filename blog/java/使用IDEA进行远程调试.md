# 使用IDEA进行远程调试

## 使用特定JVM参数运行服务端代码

要让远程服务器运行的代码支持远程调试，则启动的时候必须加上特定的JVM参数，这些参数是：

```bash
-Xdebug -Xrunjdwp:transport=dt_socket,suspend=n,server=y,address=${debug_port}
```

其中的`${debug_port}`是用户自定义的，为debug端口，本例以`5555`端口为例。

本人在这里踩过一个坑，必须要说一下。在使用公司内部的自动化部署平台NDP进行应用部署时，该平台号称支持远程调试，只需要在某个配置页面配置一下调试端口号（没有填写任何IP相关的信息），并且重新发布一下应用即可。事实上也可以发现，上述JVM参数中唯一可变的就是`${debug_port}`。但是实际在本地连接时发现却始终连不上5555 的调试端口，仔细排查才发现，下面截取了NDP发布的应用所有JVM参数列表中与远程调试相关的JVM启动参数如下：

```bash
-Xdebug -Xrunjdwp:transport=dt_socket,suspend=n,server=y,address=127.0.0.1:5555
```

将address设置为`127.0.0.1:5555`，表示将调试端口限制为本地访问，远程无法访问，这个应该是NDP平台的一个bug，我们在自己设置JVM的启动参数时也需要格外注意。

如果只是临时调试，在端口号前面不要加上限制访问的IP地址，调试完成之后，将上述JVM参数去除掉之后重新发布下，防范开放远程调试端口可能带来的安全风险。

## 本地连接远程服务器debug端口

打开Intellij IDEA，在顶部靠右的地方选择”Edit Configurations…”，进去之后点击+号，选择”Remote”，按照下图的只是填写红框内的内容，其中Name填写名称，这里为`remote webserver`，host为远程代码运行的机器的ip/hostname，port为上一步指定的debug_port，本例是`5555`。然后点击Apply，最后点击OK即可

![img](https://upload-images.jianshu.io/upload_images/2636642-e58bb8ecfa436e26.png)

## 本地IDEA启动debug模式

现在在上一步选择”Edit Configurations…”的下拉框的位置选择上一步创建的`remote webserver`，然后点击右边的`debug`按钮(长的像臭虫那个)，看控制台日志，如果出现类似“Connected to the target VM, address: ‘xx.xx.xx.xx:5555’, transport: ‘socket’”的字样，就表示连接成功过了。我这里实际显示的内容如下：



```bash
Connected to the target VM, address: '10.185.0.192:15555', transport: 'socket'
```

## 设置断点，开始调试

远程debug模式已经开启，现在可以在需要调试的代码中打断点了，比如：



![img](https:////upload-images.jianshu.io/upload_images/2636642-a4188dfdb7e5c086.png)

12.png

如图中所示，如果断点内有√，则表示选取的断点正确。

现在在本地发送一个到远程服务器的请求，看本地控制台的bug界面，划到debugger这个标签，可以看到当前远程服务的内部状态（各种变量）已经全部显示出来了，并且在刚才设置了断点的地方，也显示了该行的变量值。