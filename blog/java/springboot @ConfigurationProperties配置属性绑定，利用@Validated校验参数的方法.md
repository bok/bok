### springboot @ConfigurationProperties配置属性绑定，利用@Validated校验参数的方法



**配置对象：**

```java
package com.grace.gblog.server;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

/**
 * @author zhiqiang94@vip.qq.com
 * @version 20211213
 */
@Configuration
@EnableConfigurationProperties({AutoConfiguration.GraceUserProperties.class})
public class AutoConfiguration {

    public AutoConfiguration(GraceUserProperties config) {
    }

    @Data
    @Validated
    @RefreshScope
    @ConfigurationProperties(prefix = "grace.user")
    public static class GraceUserProperties {

        @NotBlank(message = "未配置grace.user.name")
        private String name;
        private int age = 18;

    }
}
```

**效果：**

![](https://gitee.com/zhiqiang94/image-oss/raw/master/images/gblog/202112131748583.png)

![](https://gitee.com/zhiqiang94/image-oss/raw/master/images/gblog/202112131749153.png)

