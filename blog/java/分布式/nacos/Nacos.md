# Nacos

## 概览

欢迎来到 Nacos 的世界！

Nacos 致力于帮助您发现、配置和管理微服务。Nacos 提供了一组简单易用的特性集，帮助您快速实现动态服务发现、服务配置、服务元数据及流量管理。

Nacos 帮助您更敏捷和容易地构建、交付和管理微服务平台。 Nacos 是构建以“服务”为中心的现代应用架构 (例如微服务范式、云原生范式) 的服务基础设施。

## 什么是 Nacos？

服务（Service）是 Nacos 世界的一等公民。Nacos 支持几乎所有主流类型的“服务”的发现、配置和管理：

[Kubernetes Service](https://kubernetes.io/docs/concepts/services-networking/service/)

[gRPC](https://grpc.io/docs/guides/concepts.html#service-definition) & [Dubbo RPC Service](https://dubbo.incubator.apache.org/)

[Spring Cloud RESTful Service](https://spring.io/understanding/REST)

Nacos 的关键特性包括:

- **服务发现和服务健康监测**

  Nacos 支持基于 DNS 和基于 RPC 的服务发现。服务提供者使用 [原生SDK](https://nacos.io/zh-cn/docs/sdk.html)、[OpenAPI](https://nacos.io/zh-cn/docs/open-api.html)、或一个[独立的Agent TODO](https://nacos.io/zh-cn/docs/other-language.html)注册 Service 后，服务消费者可以使用[DNS TODO](https://nacos.io/zh-cn/docs/xx) 或[HTTP&API](https://nacos.io/zh-cn/docs/open-api.html)查找和发现服务。

  Nacos 提供对服务的实时的健康检查，阻止向不健康的主机或服务实例发送请求。Nacos 支持传输层 (PING 或 TCP)和应用层 (如 HTTP、MySQL、用户自定义）的健康检查。 对于复杂的云环境和网络拓扑环境中（如 VPC、边缘网络等）服务的健康检查，Nacos 提供了 agent 上报模式和服务端主动检测2种健康检查模式。Nacos 还提供了统一的健康检查仪表盘，帮助您根据健康状态管理服务的可用性及流量。

- **动态配置服务**

  动态配置服务可以让您以中心化、外部化和动态化的方式管理所有环境的应用配置和服务配置。

  动态配置消除了配置变更时重新部署应用和服务的需要，让配置管理变得更加高效和敏捷。

  配置中心化管理让实现无状态服务变得更简单，让服务按需弹性扩展变得更容易。

  Nacos 提供了一个简洁易用的UI ([控制台样例 Demo](http://console.nacos.io/nacos/index.html)) 帮助您管理所有的服务和应用的配置。Nacos 还提供包括配置版本跟踪、金丝雀发布、一键回滚配置以及客户端配置更新状态跟踪在内的一系列开箱即用的配置管理特性，帮助您更安全地在生产环境中管理配置变更和降低配置变更带来的风险。

- **动态 DNS 服务**

  动态 DNS 服务支持权重路由，让您更容易地实现中间层负载均衡、更灵活的路由策略、流量控制以及数据中心内网的简单DNS解析服务。动态DNS服务还能让您更容易地实现以 DNS 协议为基础的服务发现，以帮助您消除耦合到厂商私有服务发现 API 上的风险。

  Nacos 提供了一些简单的 [DNS APIs TODO](https://nacos.io/zh-cn/docs/xx) 帮助您管理服务的关联域名和可用的 IP:PORT 列表.

- **服务及其元数据管理**

  Nacos 能让您从微服务平台建设的视角管理数据中心的所有服务及元数据，包括管理服务的描述、生命周期、服务的静态依赖分析、服务的健康状态、服务的流量管理、路由及安全策略、服务的 SLA 以及最首要的 metrics 统计数据。

- [更多的特性列表 ...](https://nacos.io/zh-cn/docs/roadmap.html)

## Nacos 地图

一图看懂 Nacos，下面架构部分会详细介绍。 ![nacos_map](https://nacos.io/img/nacosMap.jpg)

- 特性大图：要从功能特性，非功能特性，全面介绍我们要解的问题域的特性诉求
- 架构大图：通过清晰架构，让您快速进入 Nacos 世界
- 业务大图：利用当前特性可以支持的业务场景，及其最佳实践
- 生态大图：系统梳理 Nacos 和主流技术生态的关系
- 优势大图：展示 Nacos 核心竞争力
- 战略大图：要从战略到战术层面讲 Nacos 的宏观优势

## Nacos 生态图

![nacos_landscape.png](https://cdn.nlark.com/lark/0/2018/png/11189/1533045871534-e64b8031-008c-4dfc-b6e8-12a597a003fb.png)

如 Nacos 全景图所示，Nacos 无缝支持一些主流的开源生态，例如

- [Spring Cloud](https://nacos.io/en-us/docs/quick-start-spring-cloud.html)
- [Apache Dubbo and Dubbo Mesh](https://nacos.io/zh-cn/docs/use-nacos-with-dubbo.html)
- [Kubernetes and CNCF](https://nacos.io/zh-cn/docs/use-nacos-with-kubernetes.html)。

使用 Nacos 简化服务发现、配置管理、服务治理及管理的解决方案，让微服务的发现、管理、共享、组合更加容易。

# Nacos 概念

> NOTE: Nacos 引入了一些基本的概念，系统性的了解一下这些概念可以帮助您更好的理解和正确的使用 Nacos 产品。

## 地域

物理的数据中心，资源创建成功后不能更换。

## 可用区

同一地域内，电力和网络互相独立的物理区域。同一可用区内，实例的网络延迟较低。

## 接入点

地域的某个服务的入口域名。

## 命名空间

用于进行租户粒度的配置隔离。不同的命名空间下，可以存在相同的 Group 或 Data ID 的配置。Namespace 的常用场景之一是不同环境的配置的区分隔离，例如开发测试环境和生产环境的资源（如配置、服务）隔离等。

## 配置

在系统开发过程中，开发者通常会将一些需要变更的参数、变量等从代码中分离出来独立管理，以独立的配置文件的形式存在。目的是让静态的系统工件或者交付物（如 WAR，JAR 包等）更好地和实际的物理运行环境进行适配。配置管理一般包含在系统部署的过程中，由系统管理员或者运维人员完成。配置变更是调整系统运行时的行为的有效手段。

## 配置管理

系统配置的编辑、存储、分发、变更管理、历史版本管理、变更审计等所有与配置相关的活动。

## 配置项

一个具体的可配置的参数与其值域，通常以 param-key=param-value 的形式存在。例如我们常配置系统的日志输出级别（logLevel=INFO|WARN|ERROR） 就是一个配置项。

## 配置集

一组相关或者不相关的配置项的集合称为配置集。在系统中，一个配置文件通常就是一个配置集，包含了系统各个方面的配置。例如，一个配置集可能包含了数据源、线程池、日志级别等配置项。

## 配置集 ID

Nacos 中的某个配置集的 ID。配置集 ID 是组织划分配置的维度之一。Data ID 通常用于组织划分系统的配置集。一个系统或者应用可以包含多个配置集，每个配置集都可以被一个有意义的名称标识。Data ID 通常采用类 Java 包（如 com.taobao.tc.refund.log.level）的命名规则保证全局唯一性。此命名规则非强制。

## 配置分组

Nacos 中的一组配置集，是组织配置的维度之一。通过一个有意义的字符串（如 Buy 或 Trade ）对配置集进行分组，从而区分 Data ID 相同的配置集。当您在 Nacos 上创建一个配置时，如果未填写配置分组的名称，则配置分组的名称默认采用 DEFAULT_GROUP 。配置分组的常见场景：不同的应用或组件使用了相同的配置类型，如 database_url 配置和 MQ_topic 配置。

## 配置快照

Nacos 的客户端 SDK 会在本地生成配置的快照。当客户端无法连接到 Nacos Server 时，可以使用配置快照显示系统的整体容灾能力。配置快照类似于 Git 中的本地 commit，也类似于缓存，会在适当的时机更新，但是并没有缓存过期（expiration）的概念。

## 服务

通过预定义接口网络访问的提供给客户端的软件功能。

## 服务名

服务提供的标识，通过该标识可以唯一确定其指代的服务。

## 服务注册中心

存储服务实例和服务负载均衡策略的数据库。

## 服务发现

在计算机网络上，（通常使用服务名）对服务下的实例的地址和元数据进行探测，并以预先定义的接口提供给客户端进行查询。

## 元信息

Nacos数据（如配置和服务）描述信息，如服务版本、权重、容灾策略、负载均衡策略、鉴权配置、各种自定义标签 (label)，从作用范围来看，分为服务级别的元信息、集群的元信息及实例的元信息。

## 应用

用于标识服务提供方的服务的属性。

## 服务分组

不同的服务可以归类到同一分组。

## 虚拟集群

同一个服务下的所有服务实例组成一个默认集群, 集群可以被进一步按需求划分，划分的单位可以是虚拟集群。

## 实例

提供一个或多个服务的具有可访问网络地址（IP:Port）的进程。

## 权重

实例级别的配置。权重为浮点数。权重越大，分配给该实例的流量越大。

## 健康检查

以指定方式检查服务下挂载的实例 (Instance) 的健康度，从而确认该实例 (Instance) 是否能提供服务。根据检查结果，实例 (Instance) 会被判断为健康或不健康。对服务发起解析请求时，不健康的实例 (Instance) 不会返回给客户端。

## 健康保护阈值

为了防止因过多实例 (Instance) 不健康导致流量全部流向健康实例 (Instance) ，继而造成流量压力把健康实例 (Instance) 压垮并形成雪崩效应，应将健康保护阈值定义为一个 0 到 1 之间的浮点数。当域名健康实例数 (Instance) 占总服务实例数 (Instance) 的比例小于该值时，无论实例 (Instance) 是否健康，都会将这个实例 (Instance) 返回给客户端。这样做虽然损失了一部分流量，但是保证了集群中剩余健康实例 (Instance) 能正常工作。

# Nacos 架构

## 基本架构及概念

![nacos_arch.jpg](https://cdn.nlark.com/yuque/0/2019/jpeg/338441/1561217892717-1418fb9b-7faa-4324-87b9-f1740329f564.jpeg)

### 服务 (Service)

服务是指一个或一组软件功能（例如特定信息的检索或一组操作的执行），其目的是不同的客户端可以为不同的目的重用（例如通过跨进程的网络调用）。Nacos 支持主流的服务生态，如 Kubernetes Service、gRPC|Dubbo RPC Service 或者 Spring Cloud RESTful Service。

### 服务注册中心 (Service Registry)

服务注册中心，它是服务，其实例及元数据的数据库。服务实例在启动时注册到服务注册表，并在关闭时注销。服务和路由器的客户端查询服务注册表以查找服务的可用实例。服务注册中心可能会调用服务实例的健康检查 API 来验证它是否能够处理请求。

### 服务元数据 (Service Metadata)

服务元数据是指包括服务端点(endpoints)、服务标签、服务版本号、服务实例权重、路由规则、安全策略等描述服务的数据。

### 服务提供方 (Service Provider)

是指提供可复用和可调用服务的应用方。

### 服务消费方 (Service Consumer)

是指会发起对某个服务调用的应用方。

### 配置 (Configuration)

在系统开发过程中通常会将一些需要变更的参数、变量等从代码中分离出来独立管理，以独立的配置文件的形式存在。目的是让静态的系统工件或者交付物（如 WAR，JAR 包等）更好地和实际的物理运行环境进行适配。配置管理一般包含在系统部署的过程中，由系统管理员或者运维人员完成这个步骤。配置变更是调整系统运行时的行为的有效手段之一。

### 配置管理 (Configuration Management)

在数据中心中，系统中所有配置的编辑、存储、分发、变更管理、历史版本管理、变更审计等所有与配置相关的活动统称为配置管理。

### 名字服务 (Naming Service)

提供分布式系统中所有对象(Object)、实体(Entity)的“名字”到关联的元数据之间的映射管理服务，例如 ServiceName -> Endpoints Info, Distributed Lock Name -> Lock Owner/Status Info, DNS Domain Name -> IP List, 服务发现和 DNS 就是名字服务的2大场景。

### 配置服务 (Configuration Service)

在服务或者应用运行过程中，提供动态配置或者元数据以及配置管理的服务提供者。

### [更多概念...](https://nacos.io/zh-cn/docs/concepts.html)

## 逻辑架构及其组件介绍

![nacos-logic.jpg](https://cdn.nlark.com/yuque/0/2019/png/338441/1561217775318-6e408805-18bb-4242-b4e9-83c5b929b469.png)

- 服务管理：实现服务CRUD，域名CRUD，服务健康状态检查，服务权重管理等功能
- 配置管理：实现配置管CRUD，版本管理，灰度管理，监听管理，推送轨迹，聚合数据等功能
- 元数据管理：提供元数据CURD 和打标能力
- 插件机制：实现三个模块可分可合能力，实现扩展点SPI机制
- 事件机制：实现异步化事件通知，sdk数据变化异步通知等逻辑
- 日志模块：管理日志分类，日志级别，日志可移植性（尤其避免冲突），日志格式，异常码+帮助文档
- 回调机制：sdk通知数据，通过统一的模式回调用户处理。接口和数据结构需要具备可扩展性
- 寻址模式：解决ip，域名，nameserver、广播等多种寻址模式，需要可扩展
- 推送通道：解决server与存储、server间、server与sdk间推送性能问题
- 容量管理：管理每个租户，分组下的容量，防止存储被写爆，影响服务可用性
- 流量管理：按照租户，分组等多个维度对请求频率，长链接个数，报文大小，请求流控进行控制
- 缓存机制：容灾目录，本地缓存，server缓存机制。容灾目录使用需要工具
- 启动模式：按照单机模式，配置模式，服务模式，dns模式，或者all模式，启动不同的程序+UI
- 一致性协议：解决不同数据，不同一致性要求情况下，不同一致性机制
- 存储模块：解决数据持久化、非持久化存储，解决数据分片问题
- Nameserver：解决namespace到clusterid的路由问题，解决用户环境与nacos物理环境映射问题
- CMDB：解决元数据存储，与三方cmdb系统对接问题，解决应用，人，资源关系
- Metrics：暴露标准metrics数据，方便与三方监控系统打通
- Trace：暴露标准trace，方便与SLA系统打通，日志白平化，推送轨迹等能力，并且可以和计量计费系统打通
- 接入管理：相当于阿里云开通服务，分配身份、容量、权限过程
- 用户管理：解决用户管理，登录，sso等问题
- 权限管理：解决身份识别，访问控制，角色管理等问题
- 审计系统：扩展接口方便与不同公司审计系统打通
- 通知系统：核心数据变更，或者操作，方便通过SMS系统打通，通知到对应人数据变更
- OpenAPI：暴露标准Rest风格HTTP接口，简单易用，方便多语言集成
- Console：易用控制台，做服务管理、配置管理等操作
- SDK：多语言sdk
- Agent：dns-f类似模式，或者与mesh等方案集成
- CLI：命令行对产品进行轻量化管理，像git一样好用

## 领域模型

### 数据模型

Nacos 数据模型 Key 由三元组唯一确定, Namespace默认是空串，公共命名空间（public），分组默认是 DEFAULT_GROUP。

![nacos_data_model](https://cdn.nlark.com/yuque/0/2019/jpeg/338441/1561217857314-95ab332c-acfb-40b2-957a-aae26c2b5d71.jpeg)

### 服务领域模型

![nacos_naming_data_model](https://cdn.nlark.com/yuque/0/2019/jpeg/338441/1561217924697-ba504a35-129f-4fc6-b0df-1130b995375a.jpeg)

### 配置领域模型

围绕配置，主要有两个关联的实体，一个是配置变更历史，一个是服务标签（用于打标分类，方便索引），由 ID 关联。

![nacos_config_er](https://cdn.nlark.com/yuque/0/2019/jpeg/338441/1561217958896-4465757f-f588-4797-9c90-a76e604fabb4.jpeg)

## 类视图

### Nacos-SDK 类视图

服务部分待续

![nacos_sdk_class_relation](https://cdn.nlark.com/yuque/0/2019/jpeg/338441/1561218077514-bfa98d03-88a1-43b9-b014-1491406e3db7.jpeg)

## 构建物、部署及启动模式

![undefined](https://cdn.yuque.com/lark/0/2018/png/15914/1531730742844-e8325932-258b-49b2-9473-8d1199efe20d.png)

### 两种交付工件

Nacos 支持标准 Docker 镜像(TODO: 0.2版本开始支持）及 zip(tar.gz)压缩包的构建物。

### 两种启动模式

Nacos 支持将注册中心(Service Registry）与配置中心(Config Center) 在一个进程合并部署或者将2者分离部署的两种模式。

### 免费的公有云服务模式

除了您自己部署和启动 Nacos 服务之外，在云计算时代，Nacos 也支持公有云模式，在阿里云公有云的商业产品（如[ACM](https://www.aliyun.com/product/acm), [EDAS](https://www.aliyun.com/product/edas)) 中会提供 Nacos 的免费的公有云服务。我们也欢迎和支持其他的公有云提供商提供 Nacos 的公有云服务。

# Nacos部署环境

Nacos定义为一个IDC内部应用组件，并非面向公网环境的产品，建议在内部隔离网络环境中部署，强烈不建议部署在公共网络环境。

以下文档中提及的VIP，网卡等所有网络相关概念均处于内部网络环境。

# Nacos支持三种部署模式

- 单机模式 - 用于测试和单机试用。
- 集群模式 - 用于生产环境，确保高可用。
- 多集群模式 - 用于多数据中心场景。

## 单机模式下运行Nacos

### Linux/Unix/Mac

- Standalone means it is non-cluster Mode. * sh [startup.sh](http://startup.sh/) -m standalone

### Windows

- Standalone means it is non-cluster Mode. * cmd startup.cmd -m standalone

### 单机模式支持mysql

在0.7版本之前，在单机模式时nacos使用嵌入式数据库实现数据的存储，不方便观察数据存储的基本情况。0.7版本增加了支持mysql数据源能力，具体的操作步骤：

- 1.安装数据库，版本要求：5.6.5+
- 2.初始化mysql数据库，数据库初始化文件：nacos-mysql.sql
- 3.修改conf/application.properties文件，增加支持mysql数据源配置（目前只支持mysql），添加mysql数据源的url、用户名和密码。

```
spring.datasource.platform=mysql

db.num=1
db.url.0=jdbc:mysql://11.162.196.16:3306/nacos_devtest?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true
db.user=nacos_devtest
db.password=youdontknow
```

再以单机模式启动nacos，nacos所有写嵌入式数据库的数据都写到了mysql

## 集群模式下运行Nacos

[集群模式下运行Nacos](https://nacos.io/zh-cn/docs/cluster-mode-quick-start.html)

## 多集群模式

Nacos支持NameServer路由请求模式，通过它您可以设计一个有用的映射规则来控制请求转发到相应的集群，在映射规则中您可以按命名空间或租户等分片请求...

## 多网卡IP选择

当本地环境比较复杂的时候，Nacos服务在启动的时候需要选择运行时使用的IP或者网卡。Nacos从多网卡获取IP参考Spring Cloud设计，通过nacos.inetutils参数，可以指定Nacos使用的网卡和IP地址。目前支持的配置参数有:

- ip-address参数可以直接设置nacos的ip

```
nacos.inetutils.ip-address=10.11.105.155
```

- use-only-site-local-interfaces参数可以让nacos使用局域网ip，这个在nacos部署的机器有多网卡时很有用，可以让nacos选择局域网网卡

```
nacos.inetutils.use-only-site-local-interfaces=true
```

- ignored-interfaces支持网卡数组，可以让nacos忽略多个网卡

```
nacos.inetutils.ignored-interfaces[0]=eth0
nacos.inetutils.ignored-interfaces[1]=eth1
```

- preferred-networks参数可以让nacos优先选择匹配的ip，支持正则匹配和前缀匹配

```
nacos.inetutils.preferred-networks[0]=30.5.124.
nacos.inetutils.preferred-networks[0]=30.5.124.(25[0-5]|2[0-4]\\d|((1d{2})|([1-9]?\\d))),30.5.124.(25[0-5]|2[0-4]\\d|((1d{2})|([1-9]?\\d)))
```

# 集群部署说明

## 集群模式部署

这个快速开始手册是帮忙您快速在你的电脑上，下载安装并使用Nacos，部署生产使用的集群模式。

### 集群部署架构图

因此开源的时候推荐用户把所有服务列表放到一个vip下面，然后挂到一个域名下面

[http://ip1](http://ip1/):port/openAPI 直连ip模式，机器挂则需要修改ip才可以使用。

[http://SLB](http://slb/):port/openAPI 挂载SLB模式(内网SLB，不可暴露到公网，以免带来安全风险)，直连SLB即可，下面挂server真实ip，可读性不好。

[http://nacos.com](http://nacos.com/):port/openAPI 域名 + SLB模式(内网SLB，不可暴露到公网，以免带来安全风险)，可读性好，而且换ip方便，推荐模式

![deployDnsVipMode.jpg](https://nacos.io/img/deployDnsVipMode.jpg)

## 1. 预备环境准备

请确保是在环境中安装使用:

1. 64 bit OS Linux/Unix/Mac，推荐使用Linux系统。
2. 64 bit JDK 1.8+；[下载](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html).[配置](https://docs.oracle.com/cd/E19182-01/820-7851/inst_cli_jdk_javahome_t/)。
3. Maven 3.2.x+；[下载](https://maven.apache.org/download.cgi).[配置](https://maven.apache.org/settings.html)。
4. 3个或3个以上Nacos节点才能构成集群。

## 2. 下载源码或者安装包

你可以通过两种方式来获取 Nacos。

### 从 Github 上下载源码方式

```bash
unzip nacos-source.zip
cd nacos/
mvn -Prelease-nacos clean install -U  
cd nacos/distribution/target/nacos-server-1.3.0/nacos/bin
```

### 下载编译后压缩包方式

下载地址

[zip包](https://github.com/alibaba/nacos/releases/download/1.3.0/nacos-server-1.3.0.zip)

[tar.gz包](https://github.com/alibaba/nacos/releases/download/1.3.0/nacos-server-1.3.0.tar.gz)

```bash
  unzip nacos-server-1.3.0.zip 或者 tar -xvf nacos-server-1.3.0.tar.gz
  cd nacos/bin
```

## 3. 配置集群配置文件

在nacos的解压目录nacos/的conf目录下，有配置文件cluster.conf，请每行配置成ip:port。（请配置3个或3个以上节点）

```plain
# ip:port
200.8.9.16:8848
200.8.9.17:8848
200.8.9.18:8848
```

## 4. 确定数据源

### 使用内置数据源

无需进行任何配置

### 使用外置数据源

生产使用建议至少主备模式，或者采用高可用数据库。

#### 初始化 MySQL 数据库

[sql语句源文件](https://github.com/alibaba/nacos/blob/master/distribution/conf/nacos-mysql.sql)

#### application.properties 配置

[application.properties配置文件](https://github.com/alibaba/nacos/blob/master/distribution/conf/application.properties)

## 5. 启动服务器

### Linux/Unix/Mac

#### Stand-alone mode

```bash
sh startup.sh -m standalone
```

#### 集群模式

> 使用内置数据源

```bash
sh startup.sh -p embedded
```

> 使用外置数据源

```bash
sh startup.sh
```

## 6. 服务注册&发现和配置管理

### 服务注册

```
curl -X PUT 'http://127.0.0.1:8848/nacos/v1/ns/instance?serviceName=nacos.naming.serviceName&ip=20.18.7.10&port=8080'
```

### 服务发现

```
curl -X GET 'http://127.0.0.1:8848/nacos/v1/ns/instance/list?serviceName=nacos.naming.serviceName'
```

### 发布配置

```
curl -X POST "http://127.0.0.1:8848/nacos/v1/cs/configs?dataId=nacos.cfg.dataId&group=test&content=helloWorld"
```

### 获取配置

```
curl -X GET "http://127.0.0.1:8848/nacos/v1/cs/configs?dataId=nacos.cfg.dataId&group=test"
```

## 7. 关闭服务器

### Linux/Unix/Mac

```bash
sh shutdown.sh
```

# Nacos 快速开始

这个快速开始手册是帮忙您快速在您的电脑上，下载、安装并使用 Nacos。

## 0.版本选择

您可以在Nacos的[release notes](https://github.com/alibaba/nacos/releases)及[博客](https://nacos.io/zh-cn/blog/index.html)中找到每个版本支持的功能的介绍，当前推荐的稳定版本为2.0.3。

## 1.预备环境准备

Nacos 依赖 [Java](https://docs.oracle.com/cd/E19182-01/820-7851/inst_cli_jdk_javahome_t/) 环境来运行。如果您是从代码开始构建并运行Nacos，还需要为此配置 [Maven](https://maven.apache.org/index.html)环境，请确保是在以下版本环境中安装使用:

1. 64 bit OS，支持 Linux/Unix/Mac/Windows，推荐选用 Linux/Unix/Mac。
2. 64 bit JDK 1.8+；[下载](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) & [配置](https://docs.oracle.com/cd/E19182-01/820-7851/inst_cli_jdk_javahome_t/)。
3. Maven 3.2.x+；[下载](https://maven.apache.org/download.cgi) & [配置](https://maven.apache.org/settings.html)。

## 2.下载源码或者安装包

你可以通过源码和发行包两种方式来获取 Nacos。

### 从 Github 上下载源码方式

```bash
git clone https://github.com/alibaba/nacos.git
cd nacos/
mvn -Prelease-nacos -Dmaven.test.skip=true clean install -U  
ls -al distribution/target/

// change the $version to your actual path
cd distribution/target/nacos-server-$version/nacos/bin
```

### 下载编译后压缩包方式

您可以从 [最新稳定版本](https://github.com/alibaba/nacos/releases) 下载 `nacos-server-$version.zip` 包。

```bash
  unzip nacos-server-$version.zip 或者 tar -xvf nacos-server-$version.tar.gz
  cd nacos/bin
```

## 3.启动服务器

### Linux/Unix/Mac

启动命令(standalone代表着单机模式运行，非集群模式):

```
sh startup.sh -m standalone
```

如果您使用的是ubuntu系统，或者运行脚本报错提示[[符号找不到，可尝试如下运行：

```
bash startup.sh -m standalone
```

### Windows

启动命令(standalone代表着单机模式运行，非集群模式):

```
startup.cmd -m standalone
```

## 4.服务注册&发现和配置管理

### 服务注册

```
curl -X POST 'http://127.0.0.1:8848/nacos/v1/ns/instance?serviceName=nacos.naming.serviceName&ip=20.18.7.10&port=8080'
```

### 服务发现

```
curl -X GET 'http://127.0.0.1:8848/nacos/v1/ns/instance/list?serviceName=nacos.naming.serviceName'
```

### 发布配置

```
curl -X POST "http://127.0.0.1:8848/nacos/v1/cs/configs?dataId=nacos.cfg.dataId&group=test&content=HelloWorld"
```

### 获取配置

```
curl -X GET "http://127.0.0.1:8848/nacos/v1/cs/configs?dataId=nacos.cfg.dataId&group=test"
```

## 5.关闭服务器

### Linux/Unix/Mac

```
sh shutdown.sh
```

### Windows

```
shutdown.cmd
```

或者双击shutdown.cmd运行文件。

# Nacos Spring 快速开始

本文主要面向 Spring 的使用者，通过两个示例来介绍如何使用 Nacos 来实现分布式环境下的配置管理和服务发现。

关于 Nacos Spring 的详细文档请参看：[nacos-spring-project](https://github.com/nacos-group/nacos-spring-project/wiki/Nacos-Spring-Project-0.3.1-新功能使用手册)。

- 通过 Nacos server 和 Nacos Spring 配置管理模块，实现配置的动态变更；
- 通过 Nacos server 和 Nacos Spring 服务发现模块，实现服务的注册与发现。

## 前提条件

您需要先下载 Nacos 并启动 Nacos server。操作步骤参见 [Nacos 快速入门](https://nacos.io/zh-cn/docs/quick-start.html)。

## 启动配置管理

启动了 Nacos server 后，您就可以参考以下示例代码，为您的 Spring 应用启动 Nacos 配置管理服务了。完整示例代码请参考：[nacos-spring-config-example](https://github.com/nacos-group/nacos-examples/tree/master/nacos-spring-example/nacos-spring-config-example)

1. 添加依赖。

```
<dependency>
    <groupId>com.alibaba.nacos</groupId>
    <artifactId>nacos-spring-context</artifactId>
    <version>${latest.version}</version>
</dependency>
```

最新版本可以在 maven 仓库，如 "[mvnrepository.com](https://mvnrepository.com/artifact/com.alibaba.nacos/nacos-spring-context)" 中获取。

1. 添加 `@EnableNacosConfig` 注解启用 Nacos Spring 的配置管理服务。以下示例中，我们使用 `@NacosPropertySource` 加载了 `dataId` 为 `example` 的配置源，并开启自动更新：

```
@Configuration
@EnableNacosConfig(globalProperties = @NacosProperties(serverAddr = "127.0.0.1:8848"))
@NacosPropertySource(dataId = "example", autoRefreshed = true)
public class NacosConfiguration {

}
```

1. 通过 Nacos 的 `@NacosValue` 注解设置属性值。

```
@Controller
@RequestMapping("config")
public class ConfigController {

    @NacosValue(value = "${useLocalCache:false}", autoRefreshed = true)
    private boolean useLocalCache;

    @RequestMapping(value = "/get", method = GET)
    @ResponseBody
    public boolean get() {
        return useLocalCache;
    }
}
```

1. 启动 Tomcat，调用 `curl http://localhost:8080/config/get`尝试获取配置信息。由于此时还未发布过配置，所以返回内容是 `false`。
2. 通过调用 [Nacos Open API](https://nacos.io/zh-cn/docs/open-api.html) 向 Nacos Server 发布配置：dataId 为`example`，内容为`useLocalCache=true`

```
curl -X POST "http://127.0.0.1:8848/nacos/v1/cs/configs?dataId=example&group=DEFAULT_GROUP&content=useLocalCache=true"
```

1. 再次访问 `http://localhost:8080/config/get`，此时返回内容为`true`，说明程序中的`useLocalCache`值已经被动态更新了。

## 启动服务发现

本节演示如何在您的 Spring 项目中启动 Nacos 的服务发现功能。完整示例代码请参考：[nacos-spring-discovery-example](https://github.com/nacos-group/nacos-examples/tree/master/nacos-spring-example/nacos-spring-discovery-example)

1. 添加依赖。

```
<dependency>
    <groupId>com.alibaba.nacos</groupId>
    <artifactId>nacos-spring-context</artifactId>
    <version>${latest.version}</version>
</dependency>
```

最新版本可以在 maven 仓库，如 "[mvnrepository.com](https://mvnrepository.com/artifact/com.alibaba.nacos/nacos-spring-context)" 中获取。

1. 通过添加 `@EnableNacosDiscovery` 注解开启 Nacos Spring 的服务发现功能：

```
@Configuration
@EnableNacosDiscovery(globalProperties = @NacosProperties(serverAddr = "127.0.0.1:8848"))
public class NacosConfiguration {

}
```

1. 使用 `@NacosInjected` 注入 Nacos 的 `NamingService` 实例：

```
@Controller
@RequestMapping("discovery")
public class DiscoveryController {

    @NacosInjected
    private NamingService namingService;

    @RequestMapping(value = "/get", method = GET)
    @ResponseBody
    public List<Instance> get(@RequestParam String serviceName) throws NacosException {
        return namingService.getAllInstances(serviceName);
    }
}
```

1. 启动 Tomcat，调用 `curl http://localhost:8080/discovery/get?serviceName=example`，此时返回为空 JSON 数组`[]`。
2. 通过调用 [Nacos Open API](https://nacos.io/zh-cn/docs/open-api.html) 向 Nacos server 注册一个名称为 `example` 服务。

```
curl -X PUT 'http://127.0.0.1:8848/nacos/v1/ns/instance?serviceName=example&ip=127.0.0.1&port=8080'
```

1. 再次访问 `curl http://localhost:8080/discovery/get?serviceName=example`，此时返回内容为：

```
[
  {
    "instanceId": "127.0.0.1#8080#DEFAULT#example",
    "ip": "127.0.0.1",
    "port": 8080,
    "weight": 1.0,
    "healthy": true,
    "cluster": {
      "serviceName": null,
      "name": "",
      "healthChecker": {
        "type": "TCP"
      },
      "defaultPort": 80,
      "defaultCheckPort": 80,
      "useIPPort4Check": true,
      "metadata": {}
    },
    "service": null,
    "metadata": {}
  }
]
```

## 相关项目

- [Nacos](https://github.com/alibaba/nacos)
- [Nacos Spring](https://github.com/nacos-group/nacos-spring-project)
- [Nacos Spring Boot](https://github.com/nacos-group/nacos-spring-boot-project)
- [Spring Cloud Alibaba](https://github.com/alibaba/spring-cloud-alibaba)

# Nacos Spring Boot 快速开始

本文主要面向 Spring Boot 的使用者，通过两个示例来介绍如何使用 Nacos 来实现分布式环境下的配置管理和服务发现。

关于 Nacos Spring Boot 的详细文档请参看：[nacos-spring-boot-project](https://github.com/nacos-group/nacos-spring-boot-project/wiki/spring-boot-0.2.2-以及-0.1.2版本新功能使用手册)。

- 通过 Nacos Server 和 nacos-config-spring-boot-starter 实现配置的动态变更；
- 通过 Nacos Server 和 nacos-discovery-spring-boot-starter 实现服务的注册与发现。

## 前提条件

您需要先下载 Nacos 并启动 Nacos server。操作步骤参见 [Nacos 快速入门](https://nacos.io/zh-cn/docs/quick-start.html)。

## 启动配置管理

启动了 Nacos server 后，您就可以参考以下示例代码，为您的 Spring Boot 应用启动 Nacos 配置管理服务了。完整示例代码请参考：[nacos-spring-boot-config-example](https://github.com/nacos-group/nacos-examples/tree/master/nacos-spring-boot-example/nacos-spring-boot-config-example)

1. 添加依赖。

```
<dependency>
    <groupId>com.alibaba.boot</groupId>
    <artifactId>nacos-config-spring-boot-starter</artifactId>
    <version>${latest.version}</version>
</dependency>
```

**注意**：版本 [0.2.x.RELEASE](https://mvnrepository.com/artifact/com.alibaba.boot/nacos-config-spring-boot-starter) 对应的是 Spring Boot 2.x 版本，版本 [0.1.x.RELEASE](https://mvnrepository.com/artifact/com.alibaba.boot/nacos-config-spring-boot-starter) 对应的是 Spring Boot 1.x 版本。

1. 在 `application.properties` 中配置 Nacos server 的地址：

```
nacos.config.server-addr=127.0.0.1:8848
```

1. 使用 `@NacosPropertySource` 加载 `dataId` 为 `example` 的配置源，并开启自动更新：

```plain
@SpringBootApplication
@NacosPropertySource(dataId = "example", autoRefreshed = true)
public class NacosConfigApplication {

    public static void main(String[] args) {
        SpringApplication.run(NacosConfigApplication.class, args);
    }
}
```

1. 通过 Nacos 的 `@NacosValue` 注解设置属性值。

```
@Controller
@RequestMapping("config")
public class ConfigController {

    @NacosValue(value = "${useLocalCache:false}", autoRefreshed = true)
    private boolean useLocalCache;

    @RequestMapping(value = "/get", method = GET)
    @ResponseBody
    public boolean get() {
        return useLocalCache;
    }
}
```

1. 启动 `NacosConfigApplication`，调用 `curl http://localhost:8080/config/get`，返回内容是 `false`。
2. 通过调用 [Nacos Open API](https://nacos.io/zh-cn/docs/open-api.html) 向 Nacos server 发布配置：dataId 为`example`，内容为`useLocalCache=true`

```
curl -X POST "http://127.0.0.1:8848/nacos/v1/cs/configs?dataId=example&group=DEFAULT_GROUP&content=useLocalCache=true"
```

1. 再次访问 `http://localhost:8080/config/get`，此时返回内容为`true`，说明程序中的`useLocalCache`值已经被动态更新了。

## 启动服务发现

本节演示如何在您的 Spring Boot 项目中启动 Nacos 的服务发现功能。完整示例代码请参考：[nacos-spring-boot-discovery-example](https://github.com/nacos-group/nacos-examples/tree/master/nacos-spring-boot-example/nacos-spring-boot-discovery-example)

1. 添加依赖。

```
<dependency>
    <groupId>com.alibaba.boot</groupId>
    <artifactId>nacos-discovery-spring-boot-starter</artifactId>
    <version>${latest.version}</version>
</dependency>
```

**注意**：版本 [0.2.x.RELEASE](https://mvnrepository.com/artifact/com.alibaba.boot/nacos-discovery-spring-boot-starter) 对应的是 Spring Boot 2.x 版本，版本 [0.1.x.RELEASE](https://mvnrepository.com/artifact/com.alibaba.boot/nacos-discovery-spring-boot-starter) 对应的是 Spring Boot 1.x 版本。

1. 在 `application.properties` 中配置 Nacos server 的地址：

```
nacos.discovery.server-addr=127.0.0.1:8848
```

1. 使用 `@NacosInjected` 注入 Nacos 的 `NamingService` 实例：

```
@Controller
@RequestMapping("discovery")
public class DiscoveryController {

    @NacosInjected
    private NamingService namingService;

    @RequestMapping(value = "/get", method = GET)
    @ResponseBody
    public List<Instance> get(@RequestParam String serviceName) throws NacosException {
        return namingService.getAllInstances(serviceName);
    }
}

@SpringBootApplication
public class NacosDiscoveryApplication {

    public static void main(String[] args) {
        SpringApplication.run(NacosDiscoveryApplication.class, args);
    }
}
```

1. 启动 `NacosDiscoveryApplication`，调用 `curl http://localhost:8080/discovery/get?serviceName=example`，此时返回为空 JSON 数组`[]`。
2. 通过调用 [Nacos Open API](https://nacos.io/zh-cn/docs/open-api.html) 向 Nacos server 注册一个名称为 `example` 服务

```
curl -X PUT 'http://127.0.0.1:8848/nacos/v1/ns/instance?serviceName=example&ip=127.0.0.1&port=8080'
```

1. 再次访问 `curl http://localhost:8080/discovery/get?serviceName=example`，此时返回内容为：

```
[
  {
    "instanceId": "127.0.0.1-8080-DEFAULT-example",
    "ip": "127.0.0.1",
    "port": 8080,
    "weight": 1.0,
    "healthy": true,
    "cluster": {
      "serviceName": null,
      "name": "",
      "healthChecker": {
        "type": "TCP"
      },
      "defaultPort": 80,
      "defaultCheckPort": 80,
      "useIPPort4Check": true,
      "metadata": {}
    },
    "service": null,
    "metadata": {}
  }
]
```

## 相关项目

- [Nacos](https://github.com/alibaba/nacos)
- [Nacos Spring](https://github.com/nacos-group/nacos-spring-project)
- [Nacos Spring Boot](https://github.com/nacos-group/nacos-spring-boot-project)
- [Spring Cloud Alibaba](https://github.com/alibaba/spring-cloud-alibaba)

# Nacos Spring Cloud 快速开始

本文主要面向 [Spring Cloud](https://spring.io/projects/spring-cloud) 的使用者，通过两个示例来介绍如何使用 Nacos 来实现分布式环境下的配置管理和服务注册发现。

关于 Nacos Spring Cloud 的详细文档请参看：[Nacos Config](https://github.com/spring-cloud-incubator/spring-cloud-alibaba/wiki/Nacos-config) 和 [Nacos Discovery](https://github.com/spring-cloud-incubator/spring-cloud-alibaba/wiki/Nacos-discovery)。

- 通过 Nacos Server 和 spring-cloud-starter-alibaba-nacos-config 实现配置的动态变更。
- 通过 Nacos Server 和 spring-cloud-starter-alibaba-nacos-discovery 实现服务的注册与发现。

## 前提条件

您需要先下载 Nacos 并启动 Nacos server。操作步骤参见 [Nacos 快速入门](https://nacos.io/zh-cn/docs/quick-start.html)

## 启动配置管理

启动了 Nacos server 后，您就可以参考以下示例代码，为您的 Spring Cloud 应用启动 Nacos 配置管理服务了。完整示例代码请参考：[nacos-spring-cloud-config-example](https://github.com/nacos-group/nacos-examples/tree/master/nacos-spring-cloud-example/nacos-spring-cloud-config-example)

1. 添加依赖：

```
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-config</artifactId>
    <version>${latest.version}</version>
</dependency>
```

**注意**：版本 [2.1.x.RELEASE](https://mvnrepository.com/artifact/com.alibaba.cloud/spring-cloud-starter-alibaba-nacos-config) 对应的是 Spring Boot 2.1.x 版本。版本 [2.0.x.RELEASE](https://mvnrepository.com/artifact/com.alibaba.cloud/spring-cloud-starter-alibaba-nacos-config) 对应的是 Spring Boot 2.0.x 版本，版本 [1.5.x.RELEASE](https://mvnrepository.com/artifact/com.alibaba.cloud/spring-cloud-starter-alibaba-nacos-config) 对应的是 Spring Boot 1.5.x 版本。

更多版本对应关系参考：[版本说明 Wiki](https://github.com/spring-cloud-incubator/spring-cloud-alibaba/wiki/版本说明)

1. 在 `bootstrap.properties` 中配置 Nacos server 的地址和应用名

```
spring.cloud.nacos.config.server-addr=127.0.0.1:8848

spring.application.name=example
```

说明：之所以需要配置 `spring.application.name` ，是因为它是构成 Nacos 配置管理 `dataId`字段的一部分。

在 Nacos Spring Cloud 中，`dataId` 的完整格式如下：

```plain
${prefix}-${spring.profiles.active}.${file-extension}
```

- `prefix` 默认为 `spring.application.name` 的值，也可以通过配置项 `spring.cloud.nacos.config.prefix`来配置。
- `spring.profiles.active` 即为当前环境对应的 profile，详情可以参考 [Spring Boot文档](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-profiles.html#boot-features-profiles)。 **注意：当 `spring.profiles.active` 为空时，对应的连接符 `-` 也将不存在，dataId 的拼接格式变成 `${prefix}.${file-extension}`**
- `file-exetension` 为配置内容的数据格式，可以通过配置项 `spring.cloud.nacos.config.file-extension` 来配置。目前只支持 `properties` 和 `yaml` 类型。

1. 通过 Spring Cloud 原生注解 `@RefreshScope` 实现配置自动更新：

```
@RestController
@RequestMapping("/config")
@RefreshScope
public class ConfigController {

    @Value("${useLocalCache:false}")
    private boolean useLocalCache;

    @RequestMapping("/get")
    public boolean get() {
        return useLocalCache;
    }
}
```

1. 首先通过调用 [Nacos Open API](https://nacos.io/zh-cn/docs/open-api.html) 向 Nacos Server 发布配置：dataId 为`example.properties`，内容为`useLocalCache=true`

```
curl -X POST "http://127.0.0.1:8848/nacos/v1/cs/configs?dataId=example.properties&group=DEFAULT_GROUP&content=useLocalCache=true"
```

1. 运行 `NacosConfigApplication`，调用 `curl http://localhost:8080/config/get`，返回内容是 `true`。
2. 再次调用 [Nacos Open API](https://nacos.io/zh-cn/docs/open-api.html) 向 Nacos server 发布配置：dataId 为`example.properties`，内容为`useLocalCache=false`

```
curl -X POST "http://127.0.0.1:8848/nacos/v1/cs/configs?dataId=example.properties&group=DEFAULT_GROUP&content=useLocalCache=false"
```

1. 再次访问 `http://localhost:8080/config/get`，此时返回内容为`false`，说明程序中的`useLocalCache`值已经被动态更新了。

## 启动服务发现

本节通过实现一个简单的 `echo service` 演示如何在您的 Spring Cloud 项目中启用 Nacos 的服务发现功能，如下图示:

![echo service](https://cdn.nlark.com/lark/0/2018/png/15914/1542119181336-b6dc0fc1-ed46-43a7-9e5f-68c9ca344d60.png)

完整示例代码请参考：[nacos-spring-cloud-discovery-example](https://github.com/nacos-group/nacos-examples/tree/master/nacos-spring-cloud-example/nacos-spring-cloud-discovery-example)

1. 添加依赖：

```
<dependency>
    <groupId>com.alibaba.cloud</groupId>
    <artifactId>spring-cloud-starter-alibaba-nacos-discovery</artifactId>
    <version>${latest.version}</version>
</dependency>
```

**注意**：版本 [2.1.x.RELEASE](https://mvnrepository.com/artifact/com.alibaba.cloud/spring-cloud-starter-alibaba-nacos-discovery) 对应的是 Spring Boot 2.1.x 版本。版本 [2.0.x.RELEASE](https://mvnrepository.com/artifact/com.alibaba.cloud/spring-cloud-starter-alibaba-nacos-discovery) 对应的是 Spring Boot 2.0.x 版本，版本 [1.5.x.RELEASE](https://mvnrepository.com/artifact/com.alibaba.cloud/spring-cloud-starter-alibaba-nacos-discovery) 对应的是 Spring Boot 1.5.x 版本。

更多版本对应关系参考：[版本说明 Wiki](https://github.com/spring-cloud-incubator/spring-cloud-alibaba/wiki/版本说明)

1. 配置服务提供者，从而服务提供者可以通过 Nacos 的服务注册发现功能将其服务注册到 Nacos server 上。

i. 在 `application.properties` 中配置 Nacos server 的地址：

```
server.port=8070
spring.application.name=service-provider

spring.cloud.nacos.discovery.server-addr=127.0.0.1:8848
```

ii. 通过 Spring Cloud 原生注解 `@EnableDiscoveryClient` 开启服务注册发现功能：

```
@SpringBootApplication
@EnableDiscoveryClient
public class NacosProviderApplication {

	public static void main(String[] args) {
		SpringApplication.run(NacosProviderApplication.class, args);
	}

	@RestController
	class EchoController {
		@RequestMapping(value = "/echo/{string}", method = RequestMethod.GET)
		public String echo(@PathVariable String string) {
			return "Hello Nacos Discovery " + string;
		}
	}
}
```

1. 配置服务消费者，从而服务消费者可以通过 Nacos 的服务注册发现功能从 Nacos server 上获取到它要调用的服务。

i. 在 `application.properties` 中配置 Nacos server 的地址：

```
server.port=8080
spring.application.name=service-consumer

spring.cloud.nacos.discovery.server-addr=127.0.0.1:8848
```

ii. 通过 Spring Cloud 原生注解 `@EnableDiscoveryClient` 开启服务注册发现功能。给 [RestTemplate](https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-resttemplate.html) 实例添加`@LoadBalanced` 注解，开启 `@LoadBalanced` 与 [Ribbon](https://cloud.spring.io/spring-cloud-netflix/multi/multi_spring-cloud-ribbon.html) 的集成：

```
@SpringBootApplication
@EnableDiscoveryClient
public class NacosConsumerApplication {

    @LoadBalanced
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    public static void main(String[] args) {
        SpringApplication.run(NacosConsumerApplication.class, args);
    }

    @RestController
    public class TestController {

        private final RestTemplate restTemplate;

        @Autowired
        public TestController(RestTemplate restTemplate) {this.restTemplate = restTemplate;}

        @RequestMapping(value = "/echo/{str}", method = RequestMethod.GET)
        public String echo(@PathVariable String str) {
            return restTemplate.getForObject("http://service-provider/echo/" + str, String.class);
        }
    }
}
```

1. 启动 `ProviderApplication` 和 `ConsumerApplication` ，调用 `http://localhost:8080/echo/2018`，返回内容为 `Hello Nacos Discovery 2018`。

## 相关项目

- [Nacos](https://github.com/alibaba/nacos)
- [Nacos Spring](https://github.com/nacos-group/nacos-spring-project)
- [Nacos Spring Boot](https://github.com/nacos-group/nacos-spring-boot-project)
- [Spring Cloud Alibaba](https://github.com/alibaba/spring-cloud-alibaba)

# Java SDK

## 概述部分

Maven 坐标

```
<dependency>
    <groupId>com.alibaba.nacos</groupId>
    <artifactId>nacos-client</artifactId>
    <version>${version}</version>
</dependency>
```

## 配置管理

### 获取配置

#### 描述

用于服务启动的时候从 Nacos 获取配置。

```java
public String getConfig(String dataId, String group, long timeoutMs) throws NacosException
```

#### 请求参数

| 参数名  | 参数类型 | 描述                                                         |
| :------ | :------- | :----------------------------------------------------------- |
| dataId  | string   | 配置 ID，采用类似 package.class（如com.taobao.tc.refund.log.level）的命名规则保证全局唯一性，class 部分建议是配置的业务含义。全部字符小写。只允许英文字符和 4 种特殊字符（"."、":"、"-"、"_"），不超过 256 字节。 |
| group   | string   | 配置分组，建议填写产品名:模块名（Nacos:Test）保证唯一性，只允许英文字符和4种特殊字符（"."、":"、"-"、"_"），不超过128字节。 |
| timeout | long     | 读取配置超时时间，单位 ms，推荐值 3000。                     |

#### 返回值

| 参数类型 | 描述   |
| :------- | :----- |
| string   | 配置值 |

#### 请求示例

```java
try {
	String serverAddr = "{serverAddr}";
	String dataId = "{dataId}";
	String group = "{group}";
	Properties properties = new Properties();
	properties.put("serverAddr", serverAddr);
	ConfigService configService = NacosFactory.createConfigService(properties);
	String content = configService.getConfig(dataId, group, 5000);
	System.out.println(content);
} catch (NacosException e) {
    // TODO Auto-generated catch block
    e.printStackTrace();
}
```

#### 异常说明

读取配置超时或网络异常，抛出 NacosException 异常。

### 监听配置

#### 描述

如果希望 Nacos 推送配置变更，可以使用 Nacos 动态监听配置接口来实现。

```java
public void addListener(String dataId, String group, Listener listener) 
```

#### 请求参数

| 参数名   | 参数类型 | 描述                                                         |
| -------- | -------- | ------------------------------------------------------------ |
| dataId   | string   | 配置 ID，采用类似 package.class（如com.taobao.tc.refund.log.level）的命名规则保证全局唯一性，class 部分建议是配置的业务含义。 全部字符小写。只允许英文字符和 4 种特殊字符（"."、":"、"-"、"_"）。不超过 256 字节。 |
| group    | string   | 配置分组，建议填写**产品名：模块名**（如 Nacos:Test）保证唯一性。 只允许英文字符和4种特殊字符（"."、":"、"-"、"_"），不超过128字节。 |
| listener | Listener | 监听器，配置变更进入监听器的回调函数。                       |

#### 返回值

| 参数类型 | 描述                                                   |
| :------- | :----------------------------------------------------- |
| string   | 配置值，初始化或者配置变更的时候通过回调函数返回该值。 |

#### 请求示例

```java
String serverAddr = "{serverAddr}";
String dataId = "{dataId}";
String group = "{group}";
Properties properties = new Properties();
properties.put("serverAddr", serverAddr);
ConfigService configService = NacosFactory.createConfigService(properties);
String content = configService.getConfig(dataId, group, 5000);
System.out.println(content);
configService.addListener(dataId, group, new Listener() {
	@Override
	public void receiveConfigInfo(String configInfo) {
		System.out.println("recieve1:" + configInfo);
	}
	@Override
	public Executor getExecutor() {
		return null;
	}
});

// 测试让主线程不退出，因为订阅配置是守护线程，主线程退出守护线程就会退出。 正式代码中无需下面代码
while (true) {
    try {
        Thread.sleep(1000);
    } catch (InterruptedException e) {
        e.printStackTrace();
    }
}
```

### 删除监听

#### 描述

取消监听配置，取消监听后配置不会再推送。

```java
public void removeListener(String dataId, String group, Listener listener)
```

#### 请求参数

| 参数名   | 参数类型                    | 描述                                                         |
| :------- | :-------------------------- | :----------------------------------------------------------- |
| dataId   | string                      | 配置 ID，采用类似 package.class（如com.taobao.tc.refund.log.level）的命名规则保证全局唯一性，class 部分建议是配置的业务含义。全部字符小写。只允许英文字符和 4 种特殊字符（"."、":"、"-"、"_"），不超过 256 字节。 |
| group    | string                      | 配置分组                                                     |
| listener | ConfigChangeListenerAdapter | 监听器，配置变更进入监听器的回调函数。                       |

#### 使用示例

```java
String serverAddr = "{serverAddr}";
String dataId = "{dataId}";
String group = "{group}";
Properties properties = new Properties();
properties.put("serverAddr", serverAddr);
ConfigService configService = NacosFactory.createConfigService(properties);
configService.removeListener(dataId, group, yourListener);
```

### 发布配置

#### 描述

用于通过程序自动发布 Nacos 配置，以便通过自动化手段降低运维成本。

__注意：__创建和修改配置时使用的同一个发布接口，当配置不存在时会创建配置，当配置已存在时会更新配置。

```java
public boolean publishConfig(String dataId, String group, String content) throws NacosException;

@Since 1.4.1
public boolean publishConfig(String dataId, String group, String content, String type) throws NacosException;
```

#### 请求参数

| 参数名  | 参数类型 | 描述                                                         |
| :------ | :------- | :----------------------------------------------------------- |
| dataId  | string   | 配置 ID，采用类似 `package.class`（如 `com.taobao.tc.refund.log.level`）的命名规则保证全局唯一性。建议根据配置的业务含义来定义 class 部分。全部字符均为小写。只允许英文字符和 4 种特殊字符（“.”、“:”、“-”、“_”），不超过 256 字节。 |
| group   | string   | 配置分组，建议填写`产品名:模块名`（如 Nacos`:Test`）来保证唯一性。只允许英文字符和 4 种特殊字符（“.”、“:”、“-”、“_”），不超过 128 字节。 |
| content | string   | 配置内容，不超过 100K 字节。                                 |
| type    | string   | @Since 1.4.1. 配置类型，见 `com.alibaba.nacos.api.config.ConfigType`，默认为TEXT |

#### 返回参数

| 参数类型 | 描述         |
| :------- | :----------- |
| boolean  | 是否发布成功 |

#### 请求示例

```java
try {
    // 初始化配置服务，控制台通过示例代码自动获取下面参数
	String serverAddr = "{serverAddr}";
	String dataId = "{dataId}";
	String group = "{group}";
	Properties properties = new Properties();
	properties.put("serverAddr", serverAddr);
    ConfigService configService = NacosFactory.createConfigService(properties);
	boolean isPublishOk = configService.publishConfig(dataId, group, "content");
	System.out.println(isPublishOk);
} catch (NacosException e) {
    // TODO Auto-generated catch block
    e.printStackTrace();
}
```

#### 异常说明

读取配置超时或网络异常，抛出 NacosException 异常。

### 删除配置

#### 描述

用于通过程序自动删除 Nacos 配置，以便通过自动化手段降低运维成本。

**注意：** 当配置已存在时会删除该配置，当配置不存在时会直接返回成功消息。

```java
public boolean removeConfig(String dataId, String group) throws NacosException
```

#### 请求参数

| 参数名 | 参数类型 | 描述     |
| :----- | :------- | :------- |
| dataId | string   | 配置 ID  |
| group  | string   | 配置分组 |

#### 返回参数

| 参数类型 | 描述         |
| :------- | :----------- |
| boolean  | 是否删除成功 |

#### 请求示例

```java
try {
    // 初始化配置服务，控制台通过示例代码自动获取下面参数
	String serverAddr = "{serverAddr}";
	String dataId = "{dataId}";
	String group = "{group}";
	Properties properties = new Properties();
	properties.put("serverAddr", serverAddr);

	ConfigService configService = NacosFactory.createConfigService(properties);
	boolean isRemoveOk = configService.removeConfig(dataId, group);
	System.out.println(isRemoveOk);
} catch (NacosException e) {
    // TODO Auto-generated catch block
    e.printStackTrace();
}
```

#### 异常说明

读取配置超时或网络异常，抛出 NacosException 异常。

## 服务发现SDK

### 注册实例

#### 描述注册一个实例到服务。

```java
void registerInstance(String serviceName, String ip, int port) throws NacosException;

void registerInstance(String serviceName, String ip, int port, String clusterName) throws NacosException;

void registerInstance(String serviceName, Instance instance) throws NacosException;
```

#### 请求参数

| 名称        | 类型         | 描述         |
| :---------- | :----------- | ------------ |
| serviceName | 字符串       | 服务名       |
| ip          | 字符串       | 服务实例IP   |
| port        | int          | 服务实例port |
| clusterName | 字符串       | 集群名       |
| instance    | 参见代码注释 | 实例属性     |

#### 返回参数

无

#### 请求示例

```java
NamingService naming = NamingFactory.createNamingService(System.getProperty("serveAddr"));
naming.registerInstance("nacos.test.3", "11.11.11.11", 8888, "TEST1");

Instance instance = new Instance();
instance.setIp("55.55.55.55");
instance.setPort(9999);
instance.setHealthy(false);
instance.setWeight(2.0);
Map<String, String> instanceMeta = new HashMap<>();
instanceMeta.put("site", "et2");
instance.setMetadata(instanceMeta);

Service service = new Service("nacos.test.4");
service.setApp("nacos-naming");
service.sethealthCheckMode("server");
service.setEnableHealthCheck(true);
service.setProtectThreshold(0.8F);
service.setGroup("CNCF");
Map<String, String> serviceMeta = new HashMap<>();
serviceMeta.put("symmetricCall", "true");
service.setMetadata(serviceMeta);
instance.setService(service);

Cluster cluster = new Cluster();
cluster.setName("TEST5");
AbstractHealthChecker.Http healthChecker = new AbstractHealthChecker.Http();
healthChecker.setExpectedResponseCode(400);
healthChecker.setCurlHost("USer-Agent|Nacos");
healthChecker.setCurlPath("/xxx.html");
cluster.setHealthChecker(healthChecker);
Map<String, String> clusterMeta = new HashMap<>();
clusterMeta.put("xxx", "yyyy");
cluster.setMetadata(clusterMeta);

instance.setCluster(cluster);

naming.registerInstance("nacos.test.4", instance);
```

### 注销实例

#### 描述

删除服务下的一个实例。

```java
void deregisterInstance(String serviceName, String ip, int port) throws NacosException;

void deregisterInstance(String serviceName, String ip, int port, String clusterName) throws NacosException;
```

#### 请求参数

| 名称        | 类型   | 描述         |
| :---------- | :----- | :----------- |
| serviceName | 字符串 | 服务名       |
| ip          | 字符串 | 服务实例IP   |
| port        | int    | 服务实例port |
| clusterName | 字符串 | 集群名       |

#### 返回参数

无

#### 请求示例

```java
NamingService naming = NamingFactory.createNamingService(System.getProperty("serveAddr"));
naming.deregisterInstance("nacos.test.3", "11.11.11.11", 8888, "DEFAULT");
```

### 获取全部实例

#### 描述

获取服务下的所有实例。

```java
List<Instance> getAllInstances(String serviceName) throws NacosException;

List<Instance> getAllInstances(String serviceName, List<String> clusters) throws NacosException;
```

#### 请求参数

| 名称        | 类型   | 描述     |
| :---------- | :----- | -------- |
| serviceName | 字符串 | 服务名   |
| clusters    | List   | 集群列表 |

#### 返回参数

List 实例列表。

#### 请求示例

```java
NamingService naming = NamingFactory.createNamingService(System.getProperty("serveAddr"));
System.out.println(naming.getAllInstances("nacos.test.3"));
```

### 获取健康或不健康实例列表

#### 描述

根据条件获取过滤后的实例列表。

```java
List<Instance> selectInstances(String serviceName, boolean healthy) throws NacosException;

List<Instance> selectInstances(String serviceName, List<String> clusters, boolean healthy) throws NacosException;
```

#### 请求参数

| 名称        | 类型    | 描述     |
| :---------- | :------ | -------- |
| serviceName | 字符串  | 服务名   |
| clusters    | List    | 集群列表 |
| healthy     | boolean | 是否健康 |

#### 返回参数

List 实例列表。

#### 请求示例

```java
NamingService naming = NamingFactory.createNamingService(System.getProperty("serveAddr"));
System.out.println(naming.selectInstances("nacos.test.3", true));
```

### 获取一个健康实例

#### 描述

根据负载均衡算法随机获取一个健康实例。

```java
Instance selectOneHealthyInstance(String serviceName) throws NacosException;

Instance selectOneHealthyInstance(String serviceName, List<String> clusters) throws NacosException;
```

#### 请求参数

| 名称        | 类型   | 描述     |
| :---------- | :----- | -------- |
| serviceName | 字符串 | 服务名   |
| clusters    | List   | 集群列表 |

#### 返回参数

Instance 实例。

#### 请求示例

```java
NamingService naming = NamingFactory.createNamingService(System.getProperty("serveAddr"));
System.out.println(naming.selectOneHealthyInstance("nacos.test.3"));
```

### 监听服务

#### 描述

监听服务下的实例列表变化。

```java
void subscribe(String serviceName, EventListener listener) throws NacosException;

void subscribe(String serviceName, List<String> clusters, EventListener listener) throws NacosException;
```

#### 请求参数

| 名称        | 类型          | 描述         |
| :---------- | :------------ | ------------ |
| serviceName | 字符串        | 服务名       |
| clusters    | List          | 集群列表     |
| listener    | EventListener | 回调listener |

#### 返回参数

无

#### 请求示例

```java
NamingService naming = NamingFactory.createNamingService(System.getProperty("serveAddr"));
naming.subscribe("nacos.test.3", event -> {
    if (event instanceof NamingEvent) {
        System.out.println(((NamingEvent) event).getServceName());
        System.out.println(((NamingEvent) event).getInstances());
    }
});
```

### 取消监听服务

#### 描述

取消监听服务下的实例列表变化。

```java
void unsubscribe(String serviceName, EventListener listener) throws NacosException;

void unsubscribe(String serviceName, List<String> clusters, EventListener listener) throws NacosException;
```

#### 请求参数

| 名称        | 类型          | 描述         |
| :---------- | :------------ | ------------ |
| serviceName | 字符串        | 服务名       |
| clusters    | List          | 集群列表     |
| listener    | EventListener | 回调listener |

#### 返回参数

无

#### 请求示例

```java
NamingService naming = NamingFactory.createNamingService(System.getProperty("serveAddr"));
naming.unsubscribe("nacos.test.3", event -> {});
```

# Open API 指南

- 配置管理
  - [获取配置](https://nacos.io/zh-cn/docs/open-api.html#1.1)
  - [监听配置](https://nacos.io/zh-cn/docs/open-api.html#1.2)
  - [发布配置](https://nacos.io/zh-cn/docs/open-api.html#1.3)
  - [删除配置](https://nacos.io/zh-cn/docs/open-api.html#1.4)
  - [查询历史版本](https://nacos.io/zh-cn/docs/open-api.html#1.5)
  - [查询历史版本详情](https://nacos.io/zh-cn/docs/open-api.html#1.6)
  - [查询配置上一版本信息](https://nacos.io/zh-cn/docs/open-api.html#1.7)
- 服务发现
  - [注册实例](https://nacos.io/zh-cn/docs/open-api.html#2.1)
  - [注销实例](https://nacos.io/zh-cn/docs/open-api.html#2.2)
  - [修改实例](https://nacos.io/zh-cn/docs/open-api.html#2.3)
  - [查询实例列表](https://nacos.io/zh-cn/docs/open-api.html#2.4)
  - [查询实例详情](https://nacos.io/zh-cn/docs/open-api.html#2.5)
  - [发送实例心跳](https://nacos.io/zh-cn/docs/open-api.html#2.6)
  - [创建服务](https://nacos.io/zh-cn/docs/open-api.html#2.7)
  - [删除服务](https://nacos.io/zh-cn/docs/open-api.html#2.8)
  - [修改服务](https://nacos.io/zh-cn/docs/open-api.html#2.9)
  - [查询服务](https://nacos.io/zh-cn/docs/open-api.html#2.10)
  - [查询服务列表](https://nacos.io/zh-cn/docs/open-api.html#2.11)
  - [查询系统开关](https://nacos.io/zh-cn/docs/open-api.html#2.12)
  - [修改系统开关](https://nacos.io/zh-cn/docs/open-api.html#2.13)
  - [查看系统当前数据指标](https://nacos.io/zh-cn/docs/open-api.html#2.14)
  - [查看当前集群Server列表](https://nacos.io/zh-cn/docs/open-api.html#2.15)
  - [查看当前集群leader](https://nacos.io/zh-cn/docs/open-api.html#2.16)
  - [更新实例的健康状态](https://nacos.io/zh-cn/docs/open-api.html#2.17)
  - [批量更新实例元数据(Beta)](https://nacos.io/zh-cn/docs/open-api.html#2.18)
  - [批量删除实例元数据(Beta)](https://nacos.io/zh-cn/docs/open-api.html#2.19)
- 命名空间
  - [查询命名空间列表](https://nacos.io/zh-cn/docs/open-api.html#3.1)
  - [创建命名空间](https://nacos.io/zh-cn/docs/open-api.html#3.2)
  - [修改命名空间](https://nacos.io/zh-cn/docs/open-api.html#3.3)
  - [删除命名空间](https://nacos.io/zh-cn/docs/open-api.html#3.4)

## 配置管理

## 获取配置

### 描述

获取Nacos上的配置。

### 请求类型

GET

### 请求URL

/nacos/v1/cs/configs

### 请求参数

| 名称   | 类型   | 是否必须 | 描述                                    |
| :----- | :----- | :------- | :-------------------------------------- |
| tenant | string | 否       | 租户信息，对应 Nacos 的命名空间ID字段。 |
| dataId | string | 是       | 配置 ID。                               |
| group  | string | 是       | 配置分组。                              |

### 返回参数

| 参数类型 | 描述   |
| :------- | :----- |
| string   | 配置值 |

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例

- 请求示例

  ```plain
  curl -X GET 'http://127.0.0.1:8848/nacos/v1/cs/configs?dataId=nacos.example&group=com.alibaba.nacos'
  ```

- 返回示例

  ```
  contentTest
  ```

## 监听配置

### 描述

监听 Nacos 上的配置，以便实时感知配置变更。如果配置变更，则用[获取配置](https://nacos.io/zh-cn/docs/~~64131~~)接口获取配置的最新值，动态刷新本地缓存。

注册监听采用的是异步 Servlet 技术。注册监听本质就是带着配置和配置值的 MD5 值和后台对比。如果 MD5 值不一致，就立即返回不一致的配置。如果值一致，就等待住 30 秒。返回值为空。

### 请求类型

POST

### 请求URL

/nacos/v1/cs/configs/listener

### 请求参数

| 名称              | 类型   | 是否必须 | 描述                                                         |
| ----------------- | ------ | -------- | ------------------------------------------------------------ |
| Listening-Configs | string | 是       | 监听数据报文。格式为 dataId^2Group^2contentMD5^2tenant^1或者dataId^2Group^2contentMD5^1。dataId：配置 IDgroup：配置分组contentMD5：配置内容 MD5 值tenant：租户信息，对应 Nacos 的命名空间字段(非必填) |

### Header 参数

| 名称                 | 类型   | 是否必须 | 描述                             |
| :------------------- | :----- | :------- | :------------------------------- |
| Long-Pulling-Timeout | string | 是       | 长轮训等待 30s，此处填写 30000。 |

### 参数说明

- 配置多个字段间分隔符：^2 = Character.toString((char) 2
- 配置间分隔符：^1 = Character.toString((char) 1)
- contentMD5: MD5(content)，第一次本地缓存为空，所以这块为空串

### 返回参数

| 参数类型 | 描述   |
| :------- | :----- |
| string   | 配置值 |

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例

- 请求示例

```
http://serverIp:8848/nacos/v1/cs/configs/listener

POST 请求体数据内容：

Listening-Configs=dataId^2group^2contentMD5^2tenant^1
```

- 返回示例

```
如果配置变化

dataId^2group^2tenant^1

如果配置无变化：会返回空串
```

## 发布配置

### 描述

发布 Nacos 上的配置。

### 请求类型

POST

### 请求 URL

/nacos/v1/cs/configs

### 请求参数

| 名称    | 类型   | 是否必须 | 描述                                  |
| :------ | :----- | :------- | :------------------------------------ |
| tenant  | string | 否       | 租户信息，对应 Nacos 的命名空间ID字段 |
| dataId  | string | 是       | 配置 ID                               |
| group   | string | 是       | 配置分组                              |
| content | string | 是       | 配置内容                              |
| type    | String | 否       | 配置类型                              |

### 返回参数

| 参数类型 | 描述         |
| :------- | :----------- |
| boolean  | 是否发布成功 |

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例

- 请求示例

```
curl -X POST 'http://127.0.0.1:8848/nacos/v1/cs/configs' -d 'dataId=nacos.example&group=com.alibaba.nacos&content=contentTest'
```

- 返回示例

```
true
```

## 删除配置

### 描述

删除 Nacos 上的配置。

### 请求类型

DELETE

### 请求 URL

/nacos/v1/cs/configs

### 请求参数

| 名称   | 类型   | 是否必须 | 描述                                  |
| :----- | :----- | :------- | :------------------------------------ |
| tenant | string | 否       | 租户信息，对应 Naocs 的命名空间ID字段 |
| dataId | string | 是       | 配置 ID                               |
| group  | string | 是       | 配置分组                              |

### 返回参数

| 参数类型 | 描述         |
| :------- | :----------- |
| boolean  | 是否删除成功 |

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例

- 请求示例

```
curl -X DELETE 'http://127.0.0.1:8848/nacos/v1/cs/configs?dataId=nacos.example&group=com.alibaba.nacos'
```

- 返回示例

```
true
```

## 查询历史版本

### 描述

查询配置项历史版本。

### 请求类型

GET

### 请求 URL

/nacos/v1/cs/history?search=accurate

### 请求参数

| 名称     | 类型    | 是否必须 | 描述                                  |
| :------- | :------ | :------- | :------------------------------------ |
| tenant   | string  | 否       | 租户信息，对应 Naocs 的命名空间ID字段 |
| dataId   | string  | 是       | 配置 ID                               |
| group    | string  | 是       | 配置分组                              |
| pageNo   | integer | 否       | 当前页码                              |
| pageSize | integer | 否       | 分页条数(默认100条,最大为500)         |

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例

- 请求示例

```
curl -X GET 'http://127.0.0.1:8848/nacos/v1/cs/history?search=accurate&dataId=nacos.example&group=com.alibaba.nacos'
```

- 返回示例

```
{
  "totalCount": 1,
  "pageNumber": 1,
  "pagesAvailable": 1,
  "pageItems": [
    {
      "id": "203",
      "lastId": -1,
      "dataId": "nacos.example",
      "group": "com.alibaba.nacos",
      "tenant": "",
      "appName": "",
      "md5": null,
      "content": null,
      "srcIp": "0:0:0:0:0:0:0:1",
      "srcUser": null,
      "opType": "I         ",
      "createdTime": "2010-05-04T16:00:00.000+0000",
      "lastModifiedTime": "2020-12-05T01:48:03.380+0000"
    }
  ]
}
```

## 查询历史版本详情

### 描述

查询配置项历史版本详情

### 请求类型

GET

### 请求 URL

/nacos/v1/cs/history

### 请求参数

| 名称   | 类型    | 是否必须 | 描述                                              |
| :----- | :------ | :------- | :------------------------------------------------ |
| nid    | Integer | 是       | 配置项历史版本ID                                  |
| tenant | string  | 否       | 租户信息，对应 Naocs 的命名空间ID字段 （2.0.3起） |
| dataId | string  | 是       | 配置 ID （2.0.3起）                               |
| group  | string  | 是       | 配置分组 （2.0.3起）                              |

> 注意：2.0.3版本起，此接口需要新增字段tenant、dataId和group，其中tenant非必填。

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例

- 请求示例

```
curl -X GET 'http://127.0.0.1:8848/nacos/v1/cs/history?nid=203&tenant=&dataId=nacos.example&group=com.alibaba.nacos'
```

- 返回示例

```
{
  "id": "203",
  "lastId": -1,
  "dataId": "nacos.example",
  "group": "com.alibaba.nacos",
  "tenant": "",
  "appName": "",
  "md5": "9f67e6977b100e00cab385a75597db58",
  "content": "contentTest",
  "srcIp": "0:0:0:0:0:0:0:1",
  "srcUser": null,
  "opType": "I         ",
  "createdTime": "2010-05-04T16:00:00.000+0000",
  "lastModifiedTime": "2020-12-05T01:48:03.380+0000"
}
```

## 查询配置上一版本信息

### 描述

查询配置上一版本信息(1.4起)

### 请求类型

GET

### 请求 URL

/nacos/v1/cs/history/previous

### 请求参数

| 名称   | 类型    | 是否必须 | 描述                                              |
| :----- | :------ | :------- | :------------------------------------------------ |
| id     | Integer | 是       | 配置ID                                            |
| tenant | string  | 否       | 租户信息，对应 Naocs 的命名空间ID字段 （2.0.3起） |
| dataId | string  | 是       | 配置 ID （2.0.3起）                               |
| group  | string  | 是       | 配置分组 （2.0.3起）                              |

> 说明：2.0.3版本起，此接口需要新增字段tenant、dataId和group，其中tenant非必填。

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例

- 请求示例

```
curl -X GET 'http://127.0.0.1:8848/nacos/v1/cs/history/previous?id=309135486247505920&tenant=&dataId=nacos.example&group=com.alibaba.nacos'
```

- 返回示例

```
{
  "id": "203",
  "lastId": -1,
  "dataId": "nacos.example",
  "group": "com.alibaba.nacos",
  "tenant": "",
  "appName": "",
  "md5": "9f67e6977b100e00cab385a75597db58",
  "content": "contentTest",
  "srcIp": "0:0:0:0:0:0:0:1",
  "srcUser": null,
  "opType": "I         ",
  "createdTime": "2010-05-04T16:00:00.000+0000",
  "lastModifiedTime": "2020-12-05T01:48:03.380+0000"
}
```

## 服务发现

## 注册实例

### 描述

注册一个实例到服务。

### 请求类型

POST

### 请求路径

```plain
/nacos/v1/ns/instance
```

### 请求参数

| 名称        | 类型    | 是否必选 | 描述         |
| :---------- | :------ | :------- | ------------ |
| ip          | 字符串  | 是       | 服务实例IP   |
| port        | int     | 是       | 服务实例port |
| namespaceId | 字符串  | 否       | 命名空间ID   |
| weight      | double  | 否       | 权重         |
| enabled     | boolean | 否       | 是否上线     |
| healthy     | boolean | 否       | 是否健康     |
| metadata    | 字符串  | 否       | 扩展信息     |
| clusterName | 字符串  | 否       | 集群名       |
| serviceName | 字符串  | 是       | 服务名       |
| groupName   | 字符串  | 否       | 分组名       |
| ephemeral   | boolean | 否       | 是否临时实例 |

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例请求

```plain
curl -X POST 'http://127.0.0.1:8848/nacos/v1/ns/instance?port=8848&healthy=true&ip=11.11.11.11&weight=1.0&serviceName=nacos.test.3&encoding=GBK&namespaceId=n1'
```

### 示例返回

ok

## 注销实例

### 描述

删除服务下的一个实例。

### 请求类型

DELETE

### 请求路径

```plain
/nacos/v1/ns/instance
```

### 请求参数

| 名称        | 类型    | 是否必选 | 描述         |
| :---------- | :------ | :------- | ------------ |
| serviceName | 字符串  | 是       | 服务名       |
| groupName   | 字符串  | 否       | 分组名       |
| ip          | 字符串  | 是       | 服务实例IP   |
| port        | int     | 是       | 服务实例port |
| clusterName | 字符串  | 否       | 集群名称     |
| namespaceId | 字符串  | 否       | 命名空间ID   |
| ephemeral   | boolean | 否       | 是否临时实例 |

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例请求

```plain
curl -X DELETE '127.0.0.1:8848/nacos/v1/ns/instance?serviceName=nacos.test.1&ip=1.1.1.1&port=8888&clusterName=TEST1'
```

### 示例返回

ok

## 修改实例

### 描述

修改服务下的一个实例。

**注意：在Nacos2.0版本后，通过该接口更新的元数据拥有更高的优先级，且具有记忆能力；会在对应实例删除后，依旧存在一段时间，如果在此期间实例重新注册，该元数据依旧生效；您可以通过**`nacos.naming.clean.expired-metadata.expired-time`**及**`nacos.naming.clean.expired-metadata.interval`**对记忆时间进行修改**

### 请求类型

PUT

### 请求路径

```plain
/nacos/v1/ns/instance
```

### 请求参数

| 名称        | 类型    | 是否必选 | 描述         |
| :---------- | :------ | :------- | ------------ |
| serviceName | 字符串  | 是       | 服务名       |
| groupName   | 字符串  | 否       | 分组名       |
| ip          | 字符串  | 是       | 服务实例IP   |
| port        | int     | 是       | 服务实例port |
| clusterName | 字符串  | 否       | 集群名称     |
| namespaceId | 字符串  | 否       | 命名空间ID   |
| weight      | double  | 否       | 权重         |
| metadata    | JSON    | 否       | 扩展信息     |
| enabled     | boolean | 否       | 是否打开流量 |
| ephemeral   | boolean | 否       | 是否临时实例 |

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例请求

```plain
curl -X PUT '127.0.0.1:8848/nacos/v1/ns/instance?serviceName=nacos.test.1&ip=1.1.1.1&port=8888&clusterName=TEST1&weight=8&metadata={}'
```

### 示例返回

ok

## 查询实例列表

### 描述

查询服务下的实例列表

### 请求类型

GET

### 请求路径

```plain
/nacos/v1/ns/instance/list
```

### 请求参数

| 名称        | 类型                       | 是否必选        | 描述               |
| :---------- | :------------------------- | :-------------- | ------------------ |
| serviceName | 字符串                     | 是              | 服务名             |
| groupName   | 字符串                     | 否              | 分组名             |
| namespaceId | 字符串                     | 否              | 命名空间ID         |
| clusters    | 字符串，多个集群用逗号分隔 | 否              | 集群名称           |
| healthyOnly | boolean                    | 否，默认为false | 是否只返回健康实例 |

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例请求

```plain
curl -X GET '127.0.0.1:8848/nacos/v1/ns/instance/list?serviceName=nacos.test.1'
```

### 示例返回

```json
{
	"dom": "nacos.test.1",
	"cacheMillis": 1000,
	"useSpecifiedURL": false,
	"hosts": [{
		"valid": true,
		"marked": false,
		"instanceId": "10.10.10.10-8888-DEFAULT-nacos.test.1",
		"port": 8888,
		"ip": "10.10.10.10",
		"weight": 1.0,
		"metadata": {}
	}],
	"checksum": "3bbcf6dd1175203a8afdade0e77a27cd1528787794594",
	"lastRefTime": 1528787794594,
	"env": "",
	"clusters": ""
}
```

## 查询实例详情

### 描述

查询一个服务下个某个实例详情。

### 请求类型

GET

### 请求路径

```plain
/nacos/v1/ns/instance
```

### 请求参数

| 名称        | 类型    | 是否必选        | 描述               |
| :---------- | :------ | :-------------- | ------------------ |
| serviceName | 字符串  | 是              | 服务名             |
| groupName   | 字符串  | 否              | 分组名             |
| ip          | 字符串  | 是              | 实例IP             |
| port        | 字符串  | 是              | 实例端口           |
| namespaceId | 字符串  | 否              | 命名空间ID         |
| cluster     | 字符串  | 否              | 集群名称           |
| healthyOnly | boolean | 否，默认为false | 是否只返回健康实例 |
| ephemeral   | boolean | 否              | 是否临时实例       |

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例请求

```plain
curl -X GET '127.0.0.1:8848/nacos/v1/ns/instance?serviceName=nacos.test.2&ip=10.10.10.10&port=8888&cluster=DEFAULT'
```

### 示例返回

```json
{
	"metadata": {},
	"instanceId": "10.10.10.10-8888-DEFAULT-nacos.test.2",
	"port": 8888,
	"service": "nacos.test.2",
	"healthy": false,
	"ip": "10.10.10.10",
	"clusterName": "DEFAULT",
	"weight": 1.0
}
```

## 发送实例心跳

### 描述

发送某个实例的心跳

### 请求类型

PUT

### 请求路径

```plain
/nacos/v1/ns/instance/beat
```

### 请求参数

| 名称        | 类型           | 是否必选 | 描述         |
| :---------- | :------------- | :------- | ------------ |
| serviceName | 字符串         | 是       | 服务名       |
| groupName   | 字符串         | 否       | 分组名       |
| ephemeral   | boolean        | 否       | 是否临时实例 |
| beat        | JSON格式字符串 | 是       | 实例心跳内容 |

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例请求

```plain
curl -X PUT '127.0.0.1:8848/nacos/v1/ns/instance/beat?serviceName=nacos.test.2&beat=%7b%22cluster%22%3a%22c1%22%2c%22ip%22%3a%22127.0.0.1%22%2c%22metadata%22%3a%7b%7d%2c%22port%22%3a8080%2c%22scheduled%22%3atrue%2c%22serviceName%22%3a%22jinhan0Fx4s.173TL.net%22%2c%22weight%22%3a1%7d'
```

### 示例返回

```
ok
```

## 创建服务

### 描述

创建一个服务

### 请求类型

POST

### 请求路径

```plain
/nacos/v1/ns/service
```

### 请求参数

| 名称             | 类型           | 是否必选 | 描述                    |
| :--------------- | :------------- | :------- | ----------------------- |
| serviceName      | 字符串         | 是       | 服务名                  |
| groupName        | 字符串         | 否       | 分组名                  |
| namespaceId      | 字符串         | 否       | 命名空间ID              |
| protectThreshold | 浮点数         | 否       | 保护阈值,取值0到1,默认0 |
| metadata         | 字符串         | 否       | 元数据                  |
| selector         | JSON格式字符串 | 否       | 访问策略                |

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例请求

```plain
curl -X POST '127.0.0.1:8848/nacos/v1/ns/service?serviceName=nacos.test.2&metadata=k1%3dv1'
```

### 示例返回

```
ok
```

## 删除服务

### 描述

删除一个服务,只有当服务下实例数为0时允许删除

### 请求类型

DELETE

### 请求路径

```plain
/nacos/v1/ns/service
```

### 请求参数

| 名称        | 类型   | 是否必选 | 描述       |
| :---------- | :----- | :------- | ---------- |
| serviceName | 字符串 | 是       | 服务名     |
| groupName   | 字符串 | 否       | 分组名     |
| namespaceId | 字符串 | 否       | 命名空间ID |

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例请求

```plain
curl -X DELETE '127.0.0.1:8848/nacos/v1/ns/service?serviceName=nacos.test.2'
```

### 示例返回

```
ok
```

## 修改服务

### 描述

更新一个服务

### 请求类型

PUT

### 请求路径

```plain
/nacos/v1/ns/service
```

### 请求参数

| 名称             | 类型           | 是否必选 | 描述                    |
| :--------------- | :------------- | :------- | ----------------------- |
| serviceName      | 字符串         | 是       | 服务名                  |
| groupName        | 字符串         | 否       | 分组名                  |
| namespaceId      | 字符串         | 否       | 命名空间ID              |
| protectThreshold | 浮点数         | 否       | 保护阈值,取值0到1,默认0 |
| metadata         | 字符串         | 否       | 元数据                  |
| selector         | JSON格式字符串 | 否       | 访问策略                |

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例请求

```plain
curl -X PUT '127.0.0.1:8848/nacos/v1/ns/service?serviceName=nacos.test.2&metadata=k1%3dv1'
```

### 示例返回

```
ok
```

## 查询服务

### 描述

查询一个服务

### 请求类型

GET

### 请求路径

```plain
/nacos/v1/ns/service
```

### 请求参数

| 名称        | 类型   | 是否必选 | 描述       |
| :---------- | :----- | :------- | ---------- |
| serviceName | 字符串 | 是       | 服务名     |
| groupName   | 字符串 | 否       | 分组名     |
| namespaceId | 字符串 | 否       | 命名空间ID |

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例请求

```plain
curl -X GET '127.0.0.1:8848/nacos/v1/ns/service?serviceName=nacos.test.2'
```

### 示例返回

```
{
    metadata: { },
    groupName: "DEFAULT_GROUP",
    namespaceId: "public",
    name: "nacos.test.2",
    selector: {
        type: "none"
    },
    protectThreshold: 0,
    clusters: [
        {
            healthChecker: {
                type: "TCP"
            },
            metadata: { },
            name: "c1"
        }
    ]
}
```

## 查询服务列表

### 描述

查询服务列表

### 请求类型

GET

### 请求路径

```plain
/nacos/v1/ns/service/list
```

### 请求参数

| 名称        | 类型   | 是否必选 | 描述       |
| :---------- | :----- | :------- | ---------- |
| pageNo      | int    | 是       | 当前页码   |
| pageSize    | int    | 是       | 分页大小   |
| groupName   | 字符串 | 否       | 分组名     |
| namespaceId | 字符串 | 否       | 命名空间ID |

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例请求

```plain
curl -X GET '127.0.0.1:8848/nacos/v1/ns/service/list?pageNo=1&pageSize=2'
```

### 示例返回

```
{
    "count":148,
    "doms": [
        "nacos.test.1",
        "nacos.test.2"
    ]
}
```

## 查询系统开关

### 描述

查询系统开关

### 请求类型

GET

### 请求路径

```plain
/nacos/v1/ns/operator/switches
```

### 请求参数

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例请求

```plain
curl -X GET '127.0.0.1:8848/nacos/v1/ns/operator/switches'
```

### 示例返回

```
{
    name: "00-00---000-NACOS_SWITCH_DOMAIN-000---00-00",
    masters: null,
    adWeightMap: { },
    defaultPushCacheMillis: 10000,
    clientBeatInterval: 5000,
    defaultCacheMillis: 3000,
    distroThreshold: 0.7,
    healthCheckEnabled: true,
    distroEnabled: true,
    enableStandalone: true,
    pushEnabled: true,
    checkTimes: 3,
    httpHealthParams: {
        max: 5000,
        min: 500,
        factor: 0.85
    },
    tcpHealthParams: {
        max: 5000,
        min: 1000,
        factor: 0.75
    },
    mysqlHealthParams: {
        max: 3000,
        min: 2000,
        factor: 0.65
    },
    incrementalList: [ ],
    serverStatusSynchronizationPeriodMillis: 15000,
    serviceStatusSynchronizationPeriodMillis: 5000,
    disableAddIP: false,
    sendBeatOnly: false,
    limitedUrlMap: { },
    distroServerExpiredMillis: 30000,
    pushGoVersion: "0.1.0",
    pushJavaVersion: "0.1.0",
    pushPythonVersion: "0.4.3",
    pushCVersion: "1.0.12",
    enableAuthentication: false,
    overriddenServerStatus: "UP",
    defaultInstanceEphemeral: true,
    healthCheckWhiteList: [ ],
    checksum: null
}
```

## 修改系统开关

### 描述

修改系统开关

### 请求类型

PUT

### 请求路径

```plain
/nacos/v1/ns/operator/switches
```

### 请求参数

| 名称  | 类型    | 是否必选 | 描述                                                |
| :---- | :------ | :------- | --------------------------------------------------- |
| entry | 字符串  | 是       | 开关名                                              |
| value | 字符串  | 是       | 开关值                                              |
| debug | boolean | 否       | 是否只在本机生效,true表示本机生效,false表示集群生效 |

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例请求

```plain
curl -X PUT '127.0.0.1:8848/nacos/v1/ns/operator/switches?entry=pushEnabled&value=false&debug=true'
```

### 示例返回

```
ok
```

## 查看系统当前数据指标

### 描述

查看系统当前数据指标

### 请求类型

GET

### 请求路径

```plain
/nacos/v1/ns/operator/metrics
```

### 请求参数

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例请求

```plain
curl -X GET '127.0.0.1:8848/nacos/v1/ns/operator/metrics'
```

### 示例返回

```
{
    serviceCount: 336,
    load: 0.09,
    mem: 0.46210432,
    responsibleServiceCount: 98,
    instanceCount: 4,
    cpu: 0.010242796,
    status: "UP",
    responsibleInstanceCount: 0
}
```

## 查看当前集群Server列表

### 描述

查看当前集群Server列表

### 请求类型

GET

### 请求路径

```plain
/nacos/v1/ns/operator/servers
```

### 请求参数

| 名称    | 类型    | 是否必选 | 描述                     |
| :------ | :------ | :------- | ------------------------ |
| healthy | boolean | 否       | 是否只返回健康Server节点 |

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例请求

```plain
curl -X GET '127.0.0.1:8848/nacos/v1/ns/operator/servers'
```

### 示例返回

```
{
    servers: [
        {
            ip: "1.1.1.1",
            servePort: 8848,
            site: "unknown",
            weight: 1,
            adWeight: 0,
            alive: false,
            lastRefTime: 0,
            lastRefTimeStr: null,
            key: "1.1.1.1:8848"
        },
        {
            ip: "1.1.1.2",
            servePort: 8848,
            site: "unknown",
            weight: 1,
            adWeight: 0,
            alive: false,
            lastRefTime: 0,
            lastRefTimeStr: null,
            key: "1.1.1.2:8848"
        },
        {
            ip: "1.1.1.3",
            servePort: 8848,
            site: "unknown",
            weight: 1,
            adWeight: 0,
            alive: false,
            lastRefTime: 0,
            lastRefTimeStr: null,
            key: "1.1.1.3:8848"
        }
    ]
}
```

## 查看当前集群leader

### 描述

查看当前集群leader

### 请求类型

GET

### 请求路径

```plain
/nacos/v1/ns/raft/leader
```

### 请求参数

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例请求

```plain
curl -X GET '127.0.0.1:8848/nacos/v1/ns/raft/leader'
```

### 示例返回

```
{
    leader: "{"heartbeatDueMs":2500,"ip":"1.1.1.1:8848","leaderDueMs":12853,"state":"LEADER","term":54202,"voteFor":"1.1.1.1:8848"}"
}
```

## 更新实例的健康状态

### 描述

更新实例的健康状态,仅在集群的健康检查关闭时才生效,当集群配置了健康检查时,该接口会返回错误

### 请求类型

PUT

### 请求路径

```plain
/nacos/v1/ns/health/instance
```

### 请求参数

| 名称        | 类型    | 是否必选 | 描述         |
| :---------- | :------ | :------- | ------------ |
| namespaceId | 字符串  | 否       | 命名空间ID   |
| serviceName | 字符串  | 是       | 服务名       |
| groupName   | 字符串  | 否       | 分组名       |
| clusterName | 字符串  | 否       | 集群名       |
| ip          | 字符串  | 是       | 服务实例IP   |
| port        | int     | 是       | 服务实例port |
| healthy     | boolean | 是       | 是否健康     |

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例请求

```plain
curl -X PUT 'http://127.0.0.1:8848/nacos/v1/ns/health/instance?port=8848&healthy=true&ip=11.11.11.11&serviceName=nacos.test.3&namespaceId=n1'
```

### 示例返回

ok

## 批量更新实例元数据(Beta)

### 描述

批量更新实例元数据(1.4起)

> 注意：该接口为Beta接口，后续版本可能有所修改，甚至删除，请谨慎使用。

### 请求类型

PUT

### 请求路径

```plain
/nacos/v1/ns/instance/metadata/batch
```

### 请求参数

| 名称            | 类型           | 是否必选 | 描述                          |
| :-------------- | :------------- | :------- | ----------------------------- |
| namespaceId     | 字符串         | 是       | 命名空间ID                    |
| serviceName     | 字符串         | 是       | 服务名(group@@serviceName)    |
| consistencyType | 字符串         | 否       | 实例的类型(ephemeral/persist) |
| instances       | JSON格式字符串 | 否       | 需要更新的实例                |
| metadata        | JSON格式字符串 | 是       | 元数据信息                    |

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 参数说明

- consistencyType: 优先级高于instances参数，如果进行配置，则忽略instances参数的值。当值为'ephemeral'，则对serviceName下的所有非持久化实例进行更新。当值为'persist'，则对serviceName下的所有持久化实例进行更新。当为其他值，没有实例进行更新。
- instances: json数组。通过ip+port+ephemeral+cluster定位到某一实例。

### 示例请求

```plain
curl -X PUT 'http://localhost:8848/nacos/v1/ns/instance/metadata/batch' -d 'namespaceId=public&serviceName=xxxx@@xxxx&instances=[{"ip":"3.3.3.3","port": "8080","ephemeral":"true","clusterName":"xxxx-cluster"},{"ip":"2.2.2.2","port":"8080","ephemeral":"true","clusterName":"xxxx-cluster"}]&metadata={"age":"20","name":"cocolan"}' 
or
curl -X PUT 'http://localhost:8848/nacos/v1/ns/instance/metadata/batch' -d 'namespaceId=public&serviceName=xxxx@@xxxx&consistencyType=ephemeral&metadata={"age":"20","name":"cocolan"}'
```

### 示例返回

```
{"updated":["2.2.2.2:8080:unknown:xxxx-cluster:ephemeral","3.3.3.3:8080:unknown:xxxx-cluster:ephemeral"]}
```

## 批量删除实例元数据(Beta)

### 描述

批量删除实例元数据(1.4起)

> 注意：该接口为Beta接口，后续版本可能有所修改，甚至删除，请谨慎使用。

### 请求类型

DELETE

### 请求路径

```plain
/nacos/v1/ns/instance/metadata/batch
```

### 请求参数

| 名称            | 类型           | 是否必选 | 描述                          |
| :-------------- | :------------- | :------- | ----------------------------- |
| namespaceId     | 字符串         | 是       | 命名空间ID                    |
| serviceName     | 字符串         | 是       | 服务名(group@@serviceName)    |
| consistencyType | 字符串         | 否       | 实例的类型(ephemeral/persist) |
| instances       | JSON格式字符串 | 否       | 需要更新的实例                |
| metadata        | JSON格式字符串 | 是       | 元数据信息                    |

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 参数说明

- consistencyType: 优先级高于instances参数，如果进行配置，则忽略instances参数的值。当值为ephemeral，则对serviceName下的所有非持久化实例进行更新。当值为persist，则对serviceName下的所有持久化实例进行更新。当为其他值，没有实例进行更新。
- instances: json数组。通过ip+port+ephemeral+cluster定位到某一实例。

### 示例请求

```plain
curl -X DELETE 'http://localhost:8848/nacos/v1/ns/instance/metadata/batch' -d 'namespaceId=public&serviceName=xxxx@@xxxx&instances=[{"ip":"3.3.3.3","port": "8080","ephemeral":"true","clusterName":"xxxx-cluster"},{"ip":"2.2.2.2","port":"8080","ephemeral":"true","clusterName":"xxxx-cluster"}]&metadata={"age":"20","name":"cocolan"}' 
or
curl -X DELETE 'http://localhost:8848/nacos/v1/ns/instance/metadata/batch' -d 'namespaceId=public&serviceName=xxxx@@xxxx&consistencyType=ephemeral&metadata={"age":"20","name":"cocolan"}'
```

### 示例返回

```
{"updated":["2.2.2.2:8080:unknown:xxxx-cluster:ephemeral","3.3.3.3:8080:unknown:xxxx-cluster:ephemeral"]}
```

## 命名空间

## 查询命名空间列表

### 请求类型

GET

### 请求路径

```plain
/nacos/v1/console/namespaces
```

### 请求参数

无

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例请求

```plain
curl -X GET 'http://localhost:8848/nacos/v1/console/namespaces'
```

### 示例返回

```
{"code":200,"message":null,"data":[{"namespace":"","namespaceShowName":"public","quota":200,"configCount":0,"type":0}]}
```

## 创建命名空间

### 请求类型

POST

### 请求路径

```plain
/nacos/v1/console/namespaces
```

### 请求参数

| 名称              | 类型   | 是否必选 | 描述         |
| :---------------- | :----- | :------- | ------------ |
| customNamespaceId | 字符串 | 是       | 命名空间ID   |
| namespaceName     | 字符串 | 是       | 命名空间名   |
| namespaceDesc     | 字符串 | 否       | 命名空间描述 |

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例请求

```plain
curl -X POST 'http://localhost:8848/nacos/v1/console/namespaces' -d 'customNamespaceId=&namespaceName=dev&namespaceDesc='
```

### 示例返回

```
true
```

## 修改命名空间

### 请求类型

PUT

### 请求路径

```plain
/nacos/v1/console/namespaces
```

### 请求参数

| 名称          | 类型   | 是否必选 | 描述         |
| :------------ | :----- | :------- | ------------ |
| namespaceId   | 字符串 | 是       | 命名空间ID   |
| namespaceName | 字符串 | 是       | 命名空间名   |
| namespaceDesc | 字符串 | 是       | 命名空间描述 |

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例请求

```plain
curl -X PUT 'http://localhost:8848/nacos/v1/console/namespaces' -d 'namespace=dev&namespaceShowName=开发环境2&namespaceDesc=只用于开发2'
```

### 示例返回

```
true
```

## 删除命名空间

### 请求类型

DELETE

### 请求路径

```plain
/nacos/v1/console/namespaces
```

### 请求参数

| 名称        | 类型   | 是否必选 | 描述       |
| :---------- | :----- | :------- | ---------- |
| namespaceId | 字符串 | 是       | 命名空间ID |

### 错误编码

| 错误代码 | 描述                  | 语义                   |
| :------- | :-------------------- | :--------------------- |
| 400      | Bad Request           | 客户端请求中的语法错误 |
| 403      | Forbidden             | 没有权限               |
| 404      | Not Found             | 无法找到资源           |
| 500      | Internal Server Error | 服务器内部错误         |
| 200      | OK                    | 正常                   |

### 示例请求

```plain
curl -X DELETE 'http://localhost:8848/nacos/v1/console/namespaces' -d 'namespaceId=dev'
```

### 示例返回

```java
true
```

