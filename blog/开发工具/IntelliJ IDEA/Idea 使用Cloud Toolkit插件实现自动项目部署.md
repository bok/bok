###### Idea 使用Cloud Toolkit插件实现自动项目部署

开发工具：idea

相关插件：Alibaba Cloud Toolkit

使用效果：一键式部署、自动运行


**全图文教程，请放心食用。**



以下步骤仅第一次配置时执行

------

![](https://gitee.com/zhiqiang94/image-oss/raw/master/images/gblog/cloud_toolkit/202111051058315.png)

![](https://gitee.com/zhiqiang94/image-oss/raw/master/images/gblog/cloud_toolkit/202111051058055.png)

![](https://gitee.com/zhiqiang94/image-oss/raw/master/images/gblog/cloud_toolkit/202111051058881.png)

![](https://gitee.com/zhiqiang94/image-oss/raw/master/images/gblog/cloud_toolkit/202111051058419.png)

![](https://gitee.com/zhiqiang94/image-oss/raw/master/images/gblog/cloud_toolkit/202111051058506.png)

![](https://gitee.com/zhiqiang94/image-oss/raw/master/images/gblog/cloud_toolkit/202111051058341.png)

![](https://gitee.com/zhiqiang94/image-oss/raw/master/images/gblog/cloud_toolkit/202111051058258.png)

![](https://gitee.com/zhiqiang94/image-oss/raw/master/images/gblog/cloud_toolkit/202111051058080.png)

![](https://gitee.com/zhiqiang94/image-oss/raw/master/images/gblog/cloud_toolkit/202111051058939.png)

![](https://gitee.com/zhiqiang94/image-oss/raw/master/images/gblog/cloud_toolkit/202111051058247.png)

以下步在打包上传时执行

------

![](https://gitee.com/zhiqiang94/image-oss/raw/master/images/gblog/cloud_toolkit/202111051058321.png)

![](https://gitee.com/zhiqiang94/image-oss/raw/master/images/gblog/cloud_toolkit/202111051058356.png)

![](https://gitee.com/zhiqiang94/image-oss/raw/master/images/gblog/cloud_toolkit/202111051058373.png)

![](https://gitee.com/zhiqiang94/image-oss/raw/master/images/gblog/cloud_toolkit/202111051102506.png)

**感谢到访。**

