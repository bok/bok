# Git忽略文件中的特定行或多行

 在开发时，不可避免的在测试环境加了一些测试用的代码和自己用的测试库，为了不影响整个团队的开发和项目代码维护，在实际推送的时候不希望这些代码也同步推上去。于是就希望在提交时能忽略那几行测试代码。

方法：通过Git 的filter来过滤

**第一步**
在工程的根目录下创建/打开一个.gitattributes文件(会被提交到本地或者远程仓库)，或者在根目录下创建/.git/info/attributes（不会被提交到本地或者远程仓库）。

**第二步**
在第一步的文件（两者任选其一）中添加如下，用于定义有内容需要被过滤/忽略的文件：

```
*.java filter=_custom_config

## *.java 表示过滤所有.java 结尾的文件
## filter 是固定的,表示filter过滤器
## _custom_config 是过滤器的名称, 后面需要用到
```
这表明针对所有的java文件将要应用名为_custom_config的过滤器。

第三步
打开终端 执行命令对_custom_config过滤器进行设置

-- 配置1, 单行忽略(_custom_config 是上面配置的过滤器名称) 设置后, 添加了 //@git_ignore 结尾的代码行会被忽略提交
```
git config --global filter._custom_config.clean "sed '/\/\/@git_ignore$/'d"
git config --global filter._custom_config.smudge cat
```
-- 配置2, 多行忽略(_custom_config 是上面配置的过滤器名称) 设置后, //#git_ignore_begin 到 //#git_ignore_end 之间的代码行会被忽略提交
```
git config --global filter._custom_config.clean "sed '/\/\/#git_ignore_begin/,/\/\/#git_ignore_end$/d'"
git config --global filter._custom_config.smudge cat
```
就这样配置好, 提交代码的时候, Git 就自动会忽略了


从 Git 中删除过滤器

找到.gitconfig文件（一般在C:\Users下对应的用户文件夹里），用记事本打开，删除对应的配置项，保存即可。