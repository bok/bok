#### mysql树状结构整体排序

**MYSQL补0方式，两种情况，在字段前补0：\**LPAD(str,len,padstr)\** ，在字段后补0：RPAD(str,len,padstr)**


**1. 在字段前补0：LPAD(str,len,padstr)** 

　　LPAD(str,len,padstr) 返回字符串 str, 其左边由字符串padstr 填补到len 字符长度。假如str 的长度大于len, 则返回值被缩短至 len 字符。

 查询语句：

```
select d.deptname,
d.sort,
LPAD(d.sort,(select MAX(LENGTH(sort)) from t_sm_dept),0) as sorttemp
from t_sm_dept d ORDER BY sorttemp;
```

------

 

**2. 在字段后补0：RPAD(str,len,padstr)** 

　　RPAD(str,len,padstr) 返回字符串 str, 其右边由字符串padstr 填补到len 字符长度。假如str 的长度大于len, 则返回值被缩短至 len 字符。

查询语句（查询及排序）：

```
select d.deptname,
d.sort,
RPAD(d.SORT,(select MAX(LENGTH(sort)) from t_sm_dept),0) as sorttemp
from t_sm_dept d ORDER BY sorttemp;
```

**排序后的结果可以按照层级关系进行整体排序。**

转载自[mysql查询是对字段进行补0操作，可用于树状结构整体排序](https://www.cnblogs.com/hooly/p/10613495.html)