



# 用MySQL DISTINCT语句去重复记录



在 MySQL 查询中，可能会包含重复值。这并不成问题，不过，有时您也许希望仅仅列出不同（distinct）的值。

关键词 DISTINCT 用于返回唯一不同的值，就是[去重](http://www.nowamagic.net/academy/tag/去重)啦。用法也很简单：

```mysql
select distinct * from tableName
```

DISTINCT 这个关键字来过滤掉多余的重复记录只保留一条。

另外，如果要对某个字段去重，可以试下：

```mysql
select *, count(distinct nowamagic) from table group by nowamagic

select id,title,operatorRealName,count(distinct id,title) from test group by id,title
```

这个用法，[MySQL](http://www.nowamagic.net/academy/tag/MySQL)的版本不能太低。









